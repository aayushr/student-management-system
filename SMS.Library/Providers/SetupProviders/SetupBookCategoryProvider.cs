﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMS.Models.SetupViewModel;
using SMS.SMS.Entities;
using System.Data.Entity;

namespace SMS.Service.Providers.SetupProviders
{
    public interface ISetupBookCategoryProvider
    {
        int Save(SetupBookCategoryViewModel model);
        SetupBookCategoryViewModel Edit(int bookCategoryId);
        int Delete(int bookCategoryId);
        SetupBookCategoryViewModel GetBookCategoryList();
    }
    public class SetupBookCategoryProvider : ISetupBookCategoryProvider
    {
        private readonly SMSEntities _ent;
        public SetupBookCategoryProvider()
        {
            this._ent = new SMSEntities();
        }

        public int Delete(int bookCategoryId)
        {
            var data = _ent.SetupBookCategories.Where(x => x.BookCategoryId == bookCategoryId && x.DeletedDate == null).FirstOrDefault();
            if (data!=null)
            {
                data.DeletedBy = 1;
                data.DeletedDate = DateTime.UtcNow;
                _ent.Entry(data).State = EntityState.Modified;
                _ent.SaveChanges();
            }
            try
            {
                return 1;
            }
            catch (Exception ee)
            {

                return 0; 
            }
           
        }

        public SetupBookCategoryViewModel Edit(int bookCategoryId)
        {
            SetupBookCategoryViewModel model = new SetupBookCategoryViewModel();
            var data = _ent.SetupBookCategories.Where(x => x.BookCategoryId == bookCategoryId && x.DeletedDate == null).FirstOrDefault();
            if(data!=null)
            {
                model.BookCategoryId = data.BookCategoryId;
                model.CategoryName = data.CategoryName;
                model.Status = data.Status ?? false;
                _ent.Entry(data).State = EntityState.Modified;
                _ent.SaveChanges();
            }
            return model;
        }

        public SetupBookCategoryViewModel GetBookCategoryList()
        {
            SetupBookCategoryViewModel model = new SetupBookCategoryViewModel();
            model.SetupBookCategoryViewModelList = (from s in _ent.SetupBookCategories
                                                    where s.DeletedDate == null
                                                    select new SetupBookCategoryViewModel
                                                    {
                                                        BookCategoryId=s.BookCategoryId,
                                                        CategoryName=s.CategoryName,
                                                        Status=s.Status??false

                                                    }).ToList();
            return model;
        }

        public int Save(SetupBookCategoryViewModel model)
        {
            SetupBookCategory entitySetupBookCategory = new SetupBookCategory();
            entitySetupBookCategory.CategoryName = model.CategoryName;
              if(model.BookCategoryId>0)
            {
                entitySetupBookCategory.BookCategoryId = model.BookCategoryId;
                entitySetupBookCategory.CategoryName = model.CategoryName;
                entitySetupBookCategory.Status = model.Status;
                entitySetupBookCategory.UpdatedBy = 1;
                entitySetupBookCategory.UpdatedDate = DateTime.UtcNow;
                _ent.Entry(entitySetupBookCategory).State = EntityState.Modified;
                _ent.Entry(entitySetupBookCategory).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(entitySetupBookCategory).Property(x => x.CreatedDate).IsModified = false;
            } 
              else
            {
                entitySetupBookCategory.Status = true;
                entitySetupBookCategory.CreatedBy = 1;
                entitySetupBookCategory.CreatedDate = DateTime.UtcNow;
                _ent.Entry(entitySetupBookCategory).State = EntityState.Added;
            }
            try
            {
                _ent.SaveChanges();
                return 1;
            }
            catch (Exception ee)
            {

                return 0;
            }
            
             
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMS.Models.LibraryViewModel;
using SMS.SMS.Entities;
using System.Data.Entity;

namespace SMS.Service.Providers.LibraryProviders
{
    public interface ISetupBookProvider
    {
        int Save(SetupBookViewModel model);
        SetupBookViewModel Edit(int bookId);
        int Delete(int bookId);
        SetupBookViewModel GetBookList();
    }
    public class SetupBookProvider : ISetupBookProvider
    {
        private readonly SMSEntities _ent;
        public SetupBookProvider()
        {
            this._ent = new SMSEntities();
        }

        public int Delete(int bookId)
        {
            var data = _ent.SetupBooks.Where(x => x.BookId == bookId).FirstOrDefault();
            if (data != null)
            {
                data.DeletedBy = 1;
                data.DeletedDate = DateTime.UtcNow;
                _ent.Entry(data).State = EntityState.Modified;
                try
                {
                    _ent.SaveChanges();
                    return 1;
                }
                catch (Exception ex)
                {

                    return 0;
                }
            }
            return 0;
        }

        public SetupBookViewModel Edit(int bookId)
        {
            SetupBookViewModel model = new SetupBookViewModel();
            var data = _ent.SetupBooks.Where(x => x.BookId == bookId).FirstOrDefault();
            if (data != null)
            {
                model.BookId = data.BookId;
                model.BookName = data.BookName;
                model.BookCategoryId = data.BookCategoryId;
                model.AuthorName = data.AuthorName;
                model.Publisher = data.Publisher;
                model.Volume = data.Volume;
                model.Edition = data.Edition;
                model.Status = data.Status ?? false;
            }
            return model;
        }

        public SetupBookViewModel GetBookList()
        {

            SetupBookViewModel model = new SetupBookViewModel();
            model.SetupBookViewModelList = (from s in _ent.SetupBooks
                                            join b in _ent.SetupBookCategories on s.BookCategoryId equals b.BookCategoryId
                                            where s.DeletedDate == null
                                            select new SetupBookViewModel
                                            {
                                                BookId = s.BookId,
                                                BookName = s.BookName,
                                                BookCategoryId = s.BookCategoryId,
                                                CategoryName = b.CategoryName,
                                                AuthorName = s.AuthorName,
                                                Publisher = s.Publisher,
                                                Volume = s.Volume,
                                                Edition = s.Edition,
                                                PublishedDate = s.PublishedDate,
                                                Status = s.Status ?? false
                                            }).ToList();
            return model;
        }

        public int Save(SetupBookViewModel model)
        {
            var setupBookEntity = new SetupBook();
            setupBookEntity.BookName = model.BookName;
            setupBookEntity.BookCategoryId = model.BookCategoryId;
            setupBookEntity.AuthorName = model.AuthorName;
            setupBookEntity.Publisher = model.Publisher;
            setupBookEntity.Volume = model.Volume;
            setupBookEntity.Edition = model.Edition;


            var data = _ent.SetupBooks.Where(x => x.BookName == model.BookName.Trim() && x.BookCategoryId == model.BookCategoryId).ToList();

            if (data.Count > 0)
            {
                return 2;
            }

            if (model.BookId > 0)
            {

                setupBookEntity.BookId = model.BookId;
                setupBookEntity.UpdatedBy = 1;
                setupBookEntity.UpdatedDate = DateTime.UtcNow;
                _ent.Entry(setupBookEntity).State = EntityState.Modified;
                _ent.Entry(setupBookEntity).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(setupBookEntity).Property(x => x.CreatedDate).IsModified = false;
                _ent.Entry(setupBookEntity).Property(x => x.PublishedDate).IsModified = false;
            }
            else
            {
                //setupBookEntity.Status = true;
                setupBookEntity.PublishedDate = DateTime.UtcNow;
                setupBookEntity.Status = true;
                setupBookEntity.CreatedBy = 1;
                setupBookEntity.CreatedDate = DateTime.UtcNow;
                _ent.Entry(setupBookEntity).State = EntityState.Added;
            }
            try
            {
                _ent.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {

                return 0;
            }
        }
    }
}

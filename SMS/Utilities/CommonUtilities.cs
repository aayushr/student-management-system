﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMS.SMS.Entities;
using SMS.Models.CommonViewModel;

namespace SMS.Utilities
{
    public class CommonUtilities
    {
        public static IEnumerable<SelectListItem> GetGenderList()
        {
            return new SelectList(new[]
            {
                new {Value="M",Text="Male"},
                new {Value="F",Text="Female"},
                new {Value="O",Text="Others"},
            }, "Value", "Text");
        }

        public static IEnumerable<SelectListItem> GetStudentList()
        {
            SMSEntities ent = new SMSEntities();

            var data = (from s in ent.StudentInfoes
                        where s.DeletedDate == null
                        select new DropDownListViewModel
                        {
                            Id = s.StudentId,
                            Value = s.FirstName + " " + s.MidName + " " + s.LastName + "[" + s.StudentCode + "]",
                        }).ToList();
            return new SelectList(data, "Id", "Value");
        }

        public static IEnumerable<SelectListItem> GetClassList()
        {
            using (SMSEntities _ent = new SMSEntities())
            {
                return new SelectList(_ent.SetupClasses.Where(m => m.Status == true && m.DeletedDate==null).ToList(), "ClassId", "ClassName");
            }
        }

        public static IEnumerable<SelectListItem> GetSectionList()
        {
            using (SMSEntities _ent = new SMSEntities())
            {
                return new SelectList(_ent.SetupSections.Where(m => m.Status == true).ToList(), "SectionId", "SectionName");
            }
        }

        public static IEnumerable<SelectListItem> GetBlockList()
        {
            using (SMSEntities _ent = new SMSEntities())
            {
                return new SelectList(_ent.SetupBlocks.Where(m => m.Status == true).ToList(), "BlockId", "BlockName");
            }
        }

        public static IEnumerable<SelectListItem> GetFacultyList()
        {
            using (SMSEntities _ent = new SMSEntities())
            {
                return new SelectList(_ent.SetupFaculties.Where(m => m.Status == true && m.DeletedDate == null).ToList(), "FacultyId", "FacultyName");
            }
        }

        public static IEnumerable<SelectListItem> GetStudentType()
        {
            return new SelectList(new[]
            {
                new {Id="1",Value="Day Scholar"},
                new {Id="2",Value="Day Boarder"},
                new {Id="3",Value="Boarder"}
            }, "Value", "Value");
        }

        public static IEnumerable<SelectListItem> GetServiceList()
        {
            using (SMSEntities _ent = new SMSEntities())
            {
                return new SelectList(_ent.SetupServices.Where(m => m.Status == true).ToList(), "ServiceId", "ServiceName");
            }
        }

        public static IEnumerable<SelectListItem> GetDepartmentList()
        {
            using (SMSEntities _ent = new SMSEntities())
            {
                return new SelectList(_ent.SetupDepartments.Where(m => m.Status == true).ToList(), "DepartmentId", "DepartmentName");
            }
        }

        public static IEnumerable<SelectListItem> GetPostList()
        {
            using (SMSEntities _ent = new SMSEntities())
            {
                return new SelectList(_ent.SetupPosts.Where(m => m.Status == true).ToList(), "PostId", "PostName");
            }
        }

        public static IEnumerable<SelectListItem> GetSubjectList()
        {
            using (SMSEntities _ent = new SMSEntities())
            {
                return new SelectList(_ent.SetupSubjects.Where(m => m.Status == true).ToList(), "SubjectId", "SubjectName");
            }
        }

        public static IEnumerable<SelectListItem> GetStaffList()
        {
            using (SMSEntities _ent = new SMSEntities())
            {
                var data = (from s in _ent.StaffInfoes
                            where s.DeletedDate == null && s.Status == true
                            select new
                            {
                                StaffId = s.StaffId,
                                Name = s.FirstName + " " + s.LastName
                            }).ToList();
                return new SelectList(data, "StaffId", "Name");
            }
        }

        public static IEnumerable<SelectListItem> GetTerminaExamlList()
        {
            using (SMSEntities _ent = new SMSEntities())
            {
                return new SelectList(_ent.SetupTerminalExams.Where(m => m.Status == true).ToList(), "TerminalExamId", "Name");
            }
        }

        public static IEnumerable<SelectListItem> GetLibraryList()
        {
            using (SMSEntities _ent = new SMSEntities())
            {
                return new SelectList(_ent.SetupPaymentForLibraries.Where(m => m.Status == true).ToList(), "PaymentForLibraryId", "LibraryName");
            }
        }

        public static IEnumerable<SelectListItem> GetHostelList()
        {
            using (SMSEntities _ent = new SMSEntities())
            {
                return new SelectList(_ent.SetupPaymentForHostels.Where(m => m.Status == true).ToList(), "PaymentForHostelId", "HostelName");
            }
        }

        public static IEnumerable<SelectListItem> GetActivitiesList()
        {
            using (SMSEntities _ent = new SMSEntities())
            {
                return new SelectList(_ent.SetupPaymentForActivities.Where(m => m.Status == true).ToList(), "PaymentForActivitiesId", "ActivitiesName");
            }
        }


        public static IEnumerable<SelectListItem> GetShiftList()
        {
            return new SelectList(new[]
            {
                new {Id="1",Value="Morning"},
                new {Id="2",Value="Day"},
                new {Id="3",Value="Evening"}
            }, "Value", "Value");
        }
        public static IEnumerable<SelectListItem> GetBookCategoryList()
        {
            using (SMSEntities _ent = new SMSEntities())
            {
                return new SelectList(_ent.SetupBookCategories.Where(m => m.Status == true).ToList(), "BookCategoryId", "CategoryName");
            }
        }
    }
}
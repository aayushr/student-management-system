﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SMS.Utilities
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {

            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                filterContext.Result = new RedirectToRouteResult(
                                   new RouteValueDictionary
                               {
                                   { "action", "NotAthentication" },
                                   { "controller", "NotAthentication" }
                               });
            }

            else
                base.HandleUnauthorizedRequest(filterContext);
        }
    }
}
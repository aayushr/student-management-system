﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMS.Models.SetupViewModel;
using SMS.SMS.Entities;
using System.Data.Entity;

namespace SMS.Service.Providers.SetupProviders
{
    public interface ISetupPaymentForActivityProvider
    {
        int Save(SetupPaymentForActivityViewModel model);
        SetupPaymentForActivityViewModel Edit(int paymentForActivityId);
        SetupPaymentForActivityViewModel GetPaymentForActivityList();
        int Delete(int paymentForActivityId);
    }
    public class SetupPaymentForActivityProvider : ISetupPaymentForActivityProvider
    {
        private readonly SMSEntities _ent;
        public SetupPaymentForActivityProvider()
        {
            this._ent = new SMSEntities();
        }

        public int Delete(int paymentForActivityId)
        {
            var data = _ent.SetupPaymentForActivities.Where(x => x.PaymentForActivitiesId == paymentForActivityId).FirstOrDefault();
            if (data != null)
            {
                data.DeletedBy = 1;
                data.DeletedDate = DateTime.UtcNow;
                _ent.Entry(data).State = EntityState.Modified;
                _ent.SaveChanges();
                try
                {
                    return 1;
                }
                catch (Exception e)
                {

                    return 0;
                }
            }
            return 0;
        }

        public SetupPaymentForActivityViewModel Edit(int paymentForActivityId)
        {
            SetupPaymentForActivityViewModel model = new SetupPaymentForActivityViewModel();
            var data = _ent.SetupPaymentForActivities.Where(x => x.PaymentForActivitiesId == paymentForActivityId).FirstOrDefault();
            if (data != null)
            {
                model.PaymentForActivitiesId = data.PaymentForActivitiesId;
                model.ClassId = data.ClassId;
                model.FacultyId = data.FacultyId;
                model.ActivitiesName = data.ActivitiesName;
                model.Status = data.Status ?? false;
            }
            return model;
        }

        public SetupPaymentForActivityViewModel GetPaymentForActivityList()
        {
            SetupPaymentForActivityViewModel model = new SetupPaymentForActivityViewModel();
            model.SetupPaymentForActivityViewModelList = (from s in _ent.SetupPaymentForActivities
                                                          join f in _ent.SetupFaculties on s.FacultyId equals f.FacultyId
                                                          join c in _ent.SetupClasses on s.ClassId equals c.ClassId
                                                          where s.DeletedDate == null
                                                          select new SetupPaymentForActivityViewModel
                                                          {
                                                              PaymentForActivitiesId = s.PaymentForActivitiesId,
                                                              ActivitiesName = s.ActivitiesName,
                                                              FacultyId = s.FacultyId,
                                                              ClassId = s.ClassId,
                                                              Status = s.Status ?? false,
                                                              FacultyName = f.FacultyName,
                                                              ClassName = c.ClassName
                                                          }).ToList();
            return model;

        }

        public int Save(SetupPaymentForActivityViewModel model)
        {

            if (model.PaymentForActivitiesId == 0)
            {
                var data = _ent.SetupPaymentForActivities.Where(x => x.ActivitiesName == model.ActivitiesName.Trim() && x.ClassId == model.ClassId && x.FacultyId == model.FacultyId).ToList();
                if (data.Count > 0)
                {
                    return 2;
                }
            }

            SetupPaymentForActivity setupPaymentForActivityEntity = new SetupPaymentForActivity();
            setupPaymentForActivityEntity.ActivitiesName = model.ActivitiesName;
            setupPaymentForActivityEntity.FacultyId = model.FacultyId;
            setupPaymentForActivityEntity.ClassId = model.ClassId;
            if (model.PaymentForActivitiesId > 0)
            {
                setupPaymentForActivityEntity.Status = model.Status;
                setupPaymentForActivityEntity.PaymentForActivitiesId = model.PaymentForActivitiesId;
                setupPaymentForActivityEntity.UpdatedBy = 1;
                setupPaymentForActivityEntity.UpdatedDate = DateTime.UtcNow;
                _ent.Entry(setupPaymentForActivityEntity).State = EntityState.Modified;
                _ent.Entry(setupPaymentForActivityEntity).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(setupPaymentForActivityEntity).Property(x => x.CreatedDate).IsModified = false;

            }
            else
            {
                setupPaymentForActivityEntity.Status = true;
                setupPaymentForActivityEntity.CreatedBy = 1;
                setupPaymentForActivityEntity.CreatedDate = DateTime.UtcNow;
                _ent.Entry(setupPaymentForActivityEntity).State = EntityState.Added;

            }
            try
            {
                _ent.SaveChanges();
                return 1;
            }
            catch (Exception e)
            {

                return 0;
            }

        }
    }
}

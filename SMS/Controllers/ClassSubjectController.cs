﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMS.Service.Providers.SetupProviders;
using SMS.Models.SetupViewModel;
using SMS.Utilities;

namespace SMS.Controllers
{
    [CustomAuthorize(Roles ="SuperAdmin,Admin")]
    public class ClassSubjectController : Controller
    {
        private readonly IClassSubjectProvider _classSubjectProvider;
        private readonly ISetupSubjectProvider _setupSubjectProvider;
        public ClassSubjectController()
        {
            this._classSubjectProvider = new ClassSubjectProvider();
            this._setupSubjectProvider = new SetupSubjectProvider();
        }
        public ActionResult Index()
        {
            ClassSubjectViewModel model = new ClassSubjectViewModel();
            model = _classSubjectProvider.GetClassSubjectList();
            return View(model);
        }
        public ActionResult Create()
        {
            ClassSubjectViewModel model = new ClassSubjectViewModel();
            model.SetupSubjectViewModelList = _setupSubjectProvider.GetSubjectList().Where(x => x.IsOptional == false).ToList();
            return View(model);
        }
        [HttpPost]
         public ActionResult Create(ClassSubjectViewModel model, string[] subjectList)
        {
             int conditionApply = 1;
            foreach (var item in subjectList)
            {
                var data = new SetupSubjectViewModel();
                data.SubjectId = Convert.ToInt32(item);
                model.SetupSubjectViewModelList.Add(data);
            }
           
            if (!ModelState.IsValid)
            {

                TempData["validationError"] = Utilities.ValidationMessage.validationError;
                return RedirectToAction("Index");
            }
            else
            {
                conditionApply = _classSubjectProvider.Save(model);

                if (conditionApply == 1)
                {
                    TempData["save"] = Utilities.ValidationMessage.save;
                }
                else if(conditionApply==2)
                {
                    TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
                }
                else if(conditionApply==0)
                {
                    TempData["savefailed"] = Utilities.ValidationMessage.savefailed;
                }
            }
        
           

            return RedirectToAction("Index");
        }
        public ActionResult Edit(int? id)
        {
            ClassSubjectViewModel model = new ClassSubjectViewModel();
            model = _classSubjectProvider.Edit(id??0);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(ClassSubjectViewModel model)
        {
            int condiontApply = 0;
            if (ModelState.IsValid)
            {
                condiontApply = _classSubjectProvider.Save(model);
            }
            if (condiontApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.edit;
            }
            else if (condiontApply == 2)
            {
                TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.editfailed;
            }

            return RedirectToAction("Index");
        }
        public ActionResult Delete(int? id)
        {
            int conditionApply= _classSubjectProvider.Delete(id ?? 0);
            if(conditionApply==1)
            {
                TempData["save"] = Utilities.ValidationMessage.delete;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.deletefailed;
            }
            return RedirectToAction("Index");
        }

    }
}
﻿using SMS.Models.SetupViewModel;
using SMS.Service.Providers.SetupProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMS.Controllers
{
    public class SetupBookCategoryController : Controller
    {
        private readonly ISetupBookCategoryProvider _pro;
        public SetupBookCategoryController()
        {
            this._pro = new SetupBookCategoryProvider();
        }

        public ActionResult Index()
        {
            SetupBookCategoryViewModel model = new SetupBookCategoryViewModel();
            model = _pro.GetBookCategoryList();
            return View(model);
        }
        public ActionResult Create()
        {
            SetupBookCategoryViewModel model = new SetupBookCategoryViewModel();
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SetupBookCategoryViewModel model)
        {
            int conditionApply = 1;
            if (!ModelState.IsValid)
            {
                TempData["validationError"] = Utilities.ValidationMessage.validationError;
                return RedirectToAction("Index");
            }
            else
            {
                conditionApply = _pro.Save(model);

                if (conditionApply == 2)
                {
                    TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
                }
                else if (conditionApply == 0)
                {
                    TempData["savefailed"] = Utilities.ValidationMessage.savefailed;
                }
                else if (conditionApply == 1)
                {
                    TempData["save"] = Utilities.ValidationMessage.save;
                }
            }

            return RedirectToAction("Index");

        }
        public ActionResult Edit(int? id)
        {
            SetupBookCategoryViewModel model = new SetupBookCategoryViewModel();
            if(id!=null)
            {
                model = _pro.Edit(id??0);
            }
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SetupBookCategoryViewModel model)
        {
            int condiontApply = 0;
            if (ModelState.IsValid)
            {
                condiontApply = _pro.Save(model);
            }
            if (condiontApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.edit;
            }
            else if (condiontApply == 2)
            {
                TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.editfailed;
            }

            return RedirectToAction("Index");
        }
        public ActionResult Delete(int? id)
        {
            int condiontApply = 0;
            condiontApply = _pro.Delete(id ?? 0);
            if (condiontApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.delete;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.deletefailed;
            }
            return RedirectToAction("Index");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMS.Service.Providers.StudentInfoProvider;
using SMS.Models.StudentInfoViewModel;
using SMS.Service.Providers.StudentInfoProviders;
using SMS.Utilities;

namespace SMS.Controllers
{
    [CustomAuthorize(Roles = "SuperAdmin,Admin")]
    public class StudentClassInfoController : Controller
    {
        private readonly IStudentClassInfoProvider _pro;
        public StudentClassInfoController()
        {
            this._pro = new StudentClassInfoProvider();
        }
        public ActionResult Index()
        {
            StudentClassInfoViewModel model = new StudentClassInfoViewModel();
            model = _pro.GetList();
            return View(model);
        }

        public ActionResult Create()
        {
            StudentClassInfoViewModel model = new StudentClassInfoViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(StudentClassInfoViewModel model)
        {
            //if (ModelState.IsValid)
            //{
            //    _pro.Save(model);
            //}
            //return RedirectToAction("Index");

            int conditionApply = 1;
            if (!ModelState.IsValid)
            {
                TempData["validationError"] = Utilities.ValidationMessage.validationError;
                return RedirectToAction("Index");
            }
            else
            {
                conditionApply = _pro.Save(model);

                if (conditionApply == 2)
                {
                    TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
                }
                else if (conditionApply == 0)
                {
                    TempData["savefailed"] = Utilities.ValidationMessage.savefailed;
                }
                else if (conditionApply == 1)
                {
                    TempData["save"] = Utilities.ValidationMessage.save;
                }
            }

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? id)
        {
            StudentClassInfoViewModel model = new StudentClassInfoViewModel();
            //model = _pro.Edit(id??0);
            if (id != null)
            {
                model = _pro.Edit(id ?? 0);
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(StudentClassInfoViewModel model)
        {
            //if (ModelState.IsValid)
            //{
            //    _pro.Save(model);
            //}
            //return RedirectToAction("Index");
            int condiontApply = 0;
            if (ModelState.IsValid)
            {
                condiontApply = _pro.Save(model);
            }
            if (condiontApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.edit;
            }
            else if (condiontApply == 2)
            {
                TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.editfailed;
            }

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int? id)
        {
            //_pro.Delete(id??0);
            //return RedirectToAction("Index");

            int condiontApply = 0;
            condiontApply = _pro.Delete(id ?? 0);
            if (condiontApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.delete;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.deletefailed;
            }

            return RedirectToAction("Index");
        }

    }
}
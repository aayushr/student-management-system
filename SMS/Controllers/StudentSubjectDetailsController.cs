﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMS.Models.StudentInfoViewModel;
using SMS.Service.Providers.SetupProviders;
using SMS.Service.Providers.StudentInfoProviders;
using SMS.Models.SetupViewModel;
using SMS.Utilities;

namespace SMS.Controllers
{
    //[CustomAuthorize(Roles = "SuperAdmin,Admin")]
    public class StudentSubjectDetailsController : Controller
    {

        private readonly ISetupSubjectProvider _setupSubjectProvider;
        private readonly IStudentSubjectDetailsProviders _studentSubjectDetailsProviders;
        public StudentSubjectDetailsController()
        {
            this._setupSubjectProvider = new SetupSubjectProvider();
            this._studentSubjectDetailsProviders = new StudentSubjectDetailsProvider();
        }
        public ActionResult Index()
        {
            StudentSubjectDetailsViewModel model = new StudentSubjectDetailsViewModel();
            model = _studentSubjectDetailsProviders.GetStudentSubjectDetailsList();
            return View(model);
        }
        public ActionResult Create()
        {
            StudentSubjectDetailsViewModel model = new StudentSubjectDetailsViewModel();
            model.SetupSubjectViewModelList = _setupSubjectProvider.GetSubjectList().Where(x => x.IsOptional == true).ToList();

            return View(model);
        }
        [HttpPost]
        public ActionResult Create(StudentSubjectDetailsViewModel model, string[] subjectList)
        {
            foreach (var item in subjectList)
            {
                SetupSubjectViewModel objModel = new SetupSubjectViewModel();
                objModel.SubjectId = Convert.ToInt32(item);
                model.SetupSubjectViewModelList.Add(objModel);
            }
            _studentSubjectDetailsProviders.Save(model);
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int? id)
        {
            StudentSubjectDetailsViewModel model = new StudentSubjectDetailsViewModel();
            //model.SetupSubjectViewModelList = _setupSubjectProvider.GetSubjectList().Where(x => x.IsOptional == true).ToList();
            if (id > 0)
            {
                model = _studentSubjectDetailsProviders.Edit(id ?? 0);
            }

            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(StudentSubjectDetailsViewModel model, string[] subjectList)
        {
            foreach (var item in subjectList)
            {
                SetupSubjectViewModel objModel = new SetupSubjectViewModel();
                objModel.SubjectId = Convert.ToInt32(item);
                model.SetupSubjectViewModelList.Add(objModel);
            }
            if (ModelState.IsValid)
            {
                _studentSubjectDetailsProviders.Update(model);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int? id)
        {
            _studentSubjectDetailsProviders.Delete(id ?? 0);
            return RedirectToAction("Index");
        }

    }

   }
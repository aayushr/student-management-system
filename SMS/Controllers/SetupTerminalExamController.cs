﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMS.Service.Providers.SetupProviders;
using SMS.Models.SetupViewModel;

namespace SMS.Controllers
{
    public class SetupTerminalExamController : Controller
    {
        private readonly ISetupTerminalExamProvider _setupTerminalExamProvider;
        public SetupTerminalExamController()
        {
            this._setupTerminalExamProvider = new SetupTerminalExamProvider();
        }
        public ActionResult Index()
        {
            SetupTerminalExamViewModel model = new SetupTerminalExamViewModel();
            model = _setupTerminalExamProvider.GetTerminalExamList();
            return View(model);
        }
        public ActionResult Create()
        {
            SetupTerminalExamViewModel model = new SetupTerminalExamViewModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(SetupTerminalExamViewModel model)
        {
            int conditionApply = _setupTerminalExamProvider.Save(model);
            if (!ModelState.IsValid)
            {
                TempData["validationErro"] = Utilities.ValidationMessage.validationError;

            }

            if (conditionApply == 2)
            {
                TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;

            }
            else if (conditionApply == 0)
            {
                TempData["failed"] = Utilities.ValidationMessage.savefailed;

            }
            else
            {

                TempData["save"] = Utilities.ValidationMessage.save;
            }
            return RedirectToAction("Index");



        }
        public ActionResult Edit(int? id)
        {
            SetupTerminalExamViewModel model = new SetupTerminalExamViewModel();
            model = _setupTerminalExamProvider.Edit(id ?? 0);
            return View(model);

        }
        [HttpPost]
        public ActionResult Edit(SetupTerminalExamViewModel model)
        {
            int conditionApply = 0;
            if (ModelState.IsValid)
            {
                conditionApply = _setupTerminalExamProvider.Save(model);
            }
            if (conditionApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.edit;
            }
            else if (conditionApply == 2)
            {
                TempData["AlreadyExitsl"] = Utilities.ValidationMessage.AlreadyExits;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.savefailed;
            }
            return RedirectToAction("Index");

        }
        public ActionResult Delete(int id)
        {
            int conditionApply = 0;
            conditionApply = _setupTerminalExamProvider.Delete(id);
            if (conditionApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.delete;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.deletefailed;
            }
            return RedirectToAction("Index");
        }

    }
}
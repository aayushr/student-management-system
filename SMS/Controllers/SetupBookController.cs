﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMS.Models.LibraryViewModel;
using SMS.Service.Providers.LibraryProviders;

namespace SMS.Controllers
{
    public class SetupBookController : Controller
    {
        private readonly ISetupBookProvider _setupBookProvider;
        public SetupBookController()
        {
            this._setupBookProvider = new SetupBookProvider();
        }
        public ActionResult Index()
        {
            SetupBookViewModel model = new SetupBookViewModel();
            model = _setupBookProvider.GetBookList();
            return View(model);
        }
        public ActionResult Create()
        {
            SetupBookViewModel model = new SetupBookViewModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(SetupBookViewModel model)
        {
            int conditionApply = 1;
            if (!ModelState.IsValid)
            {
                TempData["validationError"] = Utilities.ValidationMessage.validationError;
                return RedirectToAction("Index");
            }
            else
            {
                conditionApply = _setupBookProvider.Save(model);

                if (conditionApply == 1)
                {
                    TempData["save"] = Utilities.ValidationMessage.save;
                }
                else if (conditionApply == 2)
                {
                    TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
                }
                else if (conditionApply == 0)
                {
                    TempData["savefailed"] = Utilities.ValidationMessage.savefailed;
                }
            }
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int? id)
        {
            SetupBookViewModel model = new SetupBookViewModel();
            model = _setupBookProvider.Edit(id ?? 0);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(SetupBookViewModel model)
        {
            int condiontApply = 0;
            if (ModelState.IsValid)
            {
                condiontApply = _setupBookProvider.Save(model);
            }
            if (condiontApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.edit;
            }
            else if (condiontApply == 2)
            {
                TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.editfailed;
            }

            return RedirectToAction("Index");
        }
        public ActionResult Delete(int? id)
        {
            int conditionApply = _setupBookProvider.Delete(id ?? 0);
            if (conditionApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.delete;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.deletefailed;
            }
            return RedirectToAction("Index");
        }

    }
}
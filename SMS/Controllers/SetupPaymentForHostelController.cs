﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMS.Models.SetupViewModel;
using SMS.Service.Providers.SetupProviders;
using SMS.Utilities;

namespace SMS.Controllers
{
    [CustomAuthorize(Roles = "SuperAdmin,Admin")]
    public class SetupPaymentForHostelController : Controller
    {
        private readonly ISetupPaymentForHostelProvider _pro;
        public SetupPaymentForHostelController()
        {
            this._pro = new SetupPaymentForHostelProvider();
        }
            

        public ActionResult Index()
        {
            SetupPaymentForHostelViewModel model = new SetupPaymentForHostelViewModel();
            model = _pro.GetPaymentForHostelList();
            return View(model);
        }
        public ActionResult Create()
        {
            SetupPaymentForHostelViewModel model = new SetupPaymentForHostelViewModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(SetupPaymentForHostelViewModel model)
        {
            int conditonApply = 1;
            if(!ModelState.IsValid)
            {
                TempData["validationError"] = Utilities.ValidationMessage.validationError;
                return RedirectToAction("Index");
            }
            else
            {
                conditonApply= _pro.Save(model);
                if(conditonApply==1)
                {
                    TempData["save"] = Utilities.ValidationMessage.save;

                }
                else if(conditonApply==2)
                {
                    TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
                }
                else if(conditonApply==0)
                {
                    TempData["savefailed"] = Utilities.ValidationMessage.savefailed;
                }

            }
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int? id)
        {
            SetupPaymentForHostelViewModel model = new SetupPaymentForHostelViewModel();
            if (id > 0)
            {
                model = _pro.Edit(id ?? 0);
            }
          
            return View(model);

        }
        [HttpPost]
        public ActionResult Edit(SetupPaymentForHostelViewModel model)
        {
            int conditionApply = 0;
            if(ModelState.IsValid)
            {
                conditionApply= _pro.Save(model);
            }
            if(conditionApply==1)
            {
                TempData["save"] = Utilities.ValidationMessage.edit;
            }
            else if(conditionApply==2)
            {
                TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
            }
            else 
            {
                TempData["editfailed"] = Utilities.ValidationMessage.editfailed;
            }
            return RedirectToAction("Index");
        }
        public ActionResult Delete(int? id)
        {
            int condiontApply = 0;
            condiontApply = _pro.Delete(id ?? 0);
            if (condiontApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.delete;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.deletefailed;
            }

            return RedirectToAction("Index");
        }

    }
}
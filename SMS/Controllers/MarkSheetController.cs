﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMS.Service.Providers.MarkSheetProviders;
using SMS.Models.MarkSheetViewModel;

namespace SMS.Controllers
{
    public class MarkSheetController : Controller
    {
        private readonly IMarkSheetProvider _markSheetProvider;
        public MarkSheetController()
        {
            this._markSheetProvider = new MarkSheetProvider();
        }
        public ActionResult Index()
        {
            MarkSheetViewModel model = new MarkSheetViewModel();
            model = _markSheetProvider.GetMarkSheetList();
            return View(model);
        }
        public ActionResult Create()
        {
            MarkSheetViewModel model = new MarkSheetViewModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(MarkSheetViewModel model)
        {
            int conditionApply = 1;
            if (!ModelState.IsValid)
            {
                TempData["validationError"] = Utilities.ValidationMessage.validationError;
                return RedirectToAction("Index");
            }
             conditionApply = _markSheetProvider.Save(model);
            if (conditionApply == 0)
            {
                TempData["savefailed"] = Utilities.ValidationMessage.savefailed;
            }
            else if (conditionApply == 2)
            {
                TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
            }
            else
            {
                TempData["save"] = Utilities.ValidationMessage.save;

            }
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int? id)
        {
            MarkSheetViewModel model = new MarkSheetViewModel();
            model = _markSheetProvider.Edit(id ?? 0);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(MarkSheetViewModel model)
        {
            int conditionApply = 0;
            if (ModelState.IsValid)
            {
                conditionApply = _markSheetProvider.Save(model);
            }
            if (conditionApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.edit;
            }
            else if (conditionApply == 2)
            {
                TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.editfailed;
            }



            return RedirectToAction("Index");
        }
        public ActionResult Delete(int? id)
        {
            int condiontApply = 0;
            condiontApply = _markSheetProvider.Delete(id ?? 0);

            if (condiontApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.delete;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.deletefailed;
            }

            return RedirectToAction("Index");
        }
    }
}
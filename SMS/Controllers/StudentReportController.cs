﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMS.Models.ReportModelViewModel;
using SMS.SMS.Entities;

namespace SMS.Controllers
{
    public class StudentReportController : Controller
    {
        // GET: StudentReport

        private readonly SMSEntities _ent;
        public StudentReportController()
        {
            this._ent = new SMSEntities();

        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult StudentReport()
        {
            StudentReportViewModel model = new StudentReportViewModel();
            model.StudentReportViewModelList = (from s in _ent.StudentInfoes
                                                join sc in _ent.StudentClassInfoes on s.StudentId equals sc.StudentId
                                                join cs in _ent.ClassSubjects on sc.ClassId equals cs.ClassId
                                                join sb in _ent.SetupSubjects on cs.SubjectId equals sb.SubjectId
                                                
                                                where s.DeletedDate==null
                                                select new StudentReportViewModel
                                                {
                                                   Subject=sb.SubjectName,
                                                   FullMarks=sb.FullMark??0,
                                                   PassMarks=sb.TheoryPassMarks + sb.PracticalMarks??0,
                                                 // ObtainedMarks=m.ObtainThroryMarks + m.ObtainPracticalMarks??0
                                                  
                                                }).ToList();
            return View(model);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMS.Models.SetupViewModel;
using SMS.Service.Providers.SetupProviders;

namespace SMS.Controllers
{
    public class SetupPaymentForActivityController : Controller
    {
        private readonly ISetupPaymentForActivityProvider _setupPaymentForActivityProvider;
        public SetupPaymentForActivityController()
        {
            this._setupPaymentForActivityProvider = new SetupPaymentForActivityProvider();
        }
        public ActionResult Index()
        {
            SetupPaymentForActivityViewModel model = new SetupPaymentForActivityViewModel();
            model = _setupPaymentForActivityProvider.GetPaymentForActivityList();
            return View(model);
        }
        public ActionResult Create()
        {
            SetupPaymentForActivityViewModel model = new SetupPaymentForActivityViewModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(SetupPaymentForActivityViewModel model)
        {
            int validationApply = 1;
            if (!ModelState.IsValid)
            {
                TempData["validationError"] = Utilities.ValidationMessage.validationError;
                return RedirectToAction("Index");
            }
            else
            {
                validationApply = _setupPaymentForActivityProvider.Save(model);
                if (validationApply == 0)
                {
                    TempData["savefailed"] = Utilities.ValidationMessage.savefailed;
                }

                else if (validationApply == 2)
                {
                    TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
                }
                else
                {
                    TempData["save"] = Utilities.ValidationMessage.save;

                }

            }

            return RedirectToAction("Index");
        }
        public ActionResult Edit(int? id)
        {
            SetupPaymentForActivityViewModel model = new SetupPaymentForActivityViewModel();
            if (id > 0)
            {
                model = _setupPaymentForActivityProvider.Edit(id ?? 0);
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(SetupPaymentForActivityViewModel model)
        {
            int validationApply = 0;
            if (ModelState.IsValid)
            {
                validationApply = _setupPaymentForActivityProvider.Save(model);
            }
            if (validationApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.edit;
            }
            else if (validationApply == 2)
            {
                TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.editfailed;
            }
            return RedirectToAction("Index");
        }
        public ActionResult Delete(int? id)
        {
            int validationApply = 0;
            validationApply = _setupPaymentForActivityProvider.Delete(id ?? 0);
            if (validationApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.delete;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.deletefailed;
            }
            return RedirectToAction("Index");
        }
    }
}
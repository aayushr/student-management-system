﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMS.Models.SetupViewModel;
using SMS.Service.Providers.SetupProviders;
using SMS.Utilities;

namespace SMS.Controllers
{
    [CustomAuthorize(Roles = "SuperAdmin,Admin")]
    public class SetupPostController : Controller
    {
        private readonly ISetupPostProvider _pro;
        public SetupPostController()
        {
            this._pro = new SetupPostProvider();
        }

        public ActionResult Index()
        {
            SetupPostViewModel model = new SetupPostViewModel();
            model = _pro.GetPostList();
            return View(model);
        }
        public ActionResult Create()
        {
            SetupPostViewModel model = new SetupPostViewModel();

            return View(model);

        }
        [HttpPost]
        public ActionResult Create(SetupPostViewModel model)
        {
            int conditionApply = 1;
            if (!ModelState.IsValid)
            {
                TempData["validationError"] = Utilities.ValidationMessage.validationError;
                return RedirectToAction("Index");
            }
            else
            {
                conditionApply = _pro.Save(model);

                if (conditionApply == 2)
                {
                    TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
                }
                else if (conditionApply == 0)
                {
                    TempData["savefailed"] = Utilities.ValidationMessage.savefailed;
                }
                else if (conditionApply == 1)
                {
                    TempData["save"] = Utilities.ValidationMessage.save;
                }
            }

            return RedirectToAction("Index");
        }
        public ActionResult Edit(int? id)
        {
            SetupPostViewModel model = new SetupPostViewModel();
            if (id != null)
            {
                model = _pro.Edit(id ?? 0);
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(SetupPostViewModel model)
        {
            int condiontApply = 0;
            if (ModelState.IsValid)
            {
                condiontApply = _pro.Save(model);
            }
            if (condiontApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.edit;
            }
            else if (condiontApply == 2)
            {
                TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.editfailed;
            }

            return RedirectToAction("Index");
        }
        public ActionResult Delete(int? id)
        {
            int condiontApply = 0;
            condiontApply = _pro.Delete(id ?? 0);
            if (condiontApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.delete;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.deletefailed;
            }

            return RedirectToAction("Index");
        }
    }
}
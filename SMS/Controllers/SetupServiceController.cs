﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMS.Models.SetupViewModel;
using SMS.Service.Providers.SetupProviders;
using SMS.Utilities;

namespace SMS.Controllers
{
    [CustomAuthorize(Roles = "SuperAdmin,Admin")]
    public class SetupServiceController : Controller
    {
        private readonly ISetupServiceProvider _pro;
        public SetupServiceController()
        {
            this._pro = new SetupServiceProvider();
        }
            
        public ActionResult Index()
        {
            SetupServiceViewModel model = new SetupServiceViewModel();
            model = _pro.GetServiceList();
            return View(model);
        }
        public ActionResult Create()
        {
            SetupServiceViewModel model = new SetupServiceViewModel();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SetupServiceViewModel model)
        {
            int conditionApply = 1;
            if (!ModelState.IsValid)
            {
                TempData["validationError"] = Utilities.ValidationMessage.validationError;
                return RedirectToAction("Index");
            }
            else
            {
                conditionApply = _pro.Save(model);

                if (conditionApply == 2)
                {
                    TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
                }

                else if (conditionApply == 1)
                {
                    TempData["save"] = Utilities.ValidationMessage.save;
                }
                else if (conditionApply == 0)
                {
                    TempData["savefailed"] = Utilities.ValidationMessage.savefailed;
                }
            }

            return RedirectToAction("Index");
           
        }
        public ActionResult Edit(int? id)
        {
            SetupServiceViewModel model = new SetupServiceViewModel();
            if (id >0)
            {
                model = _pro.Edit(id ?? 0);
            }
            
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SetupServiceViewModel model)
        {

            int condiontApply = 0;
            if (ModelState.IsValid)
            {
                condiontApply = _pro.Save(model);
            }
            if (condiontApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.edit;
            }
            else if (condiontApply == 2)
            {
                TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.editfailed;
            }

            return RedirectToAction("Index");

        }
        public ActionResult Delete(int? id)
        {
            int condiontApply = 0;
            condiontApply = _pro.Delete(id ?? 0);
            if (condiontApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.delete;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.deletefailed;
            }
            return RedirectToAction("Index");
        }
    }
}
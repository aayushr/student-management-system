﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMS.Models.SetupViewModel;
using SMS.Service.Providers.SetupProviders;
using SMS.Utilities;

namespace SMS.Controllers
{
    [CustomAuthorize(Roles = "SuperAdmin,Admin")]
    public class SetupDepartmentController : Controller
    {
        private readonly ISetupDepartmentProvider _setupDepartmentProvider;
        public SetupDepartmentController()
        {
            _setupDepartmentProvider = new SetupDepartmentProvider();
        }
        public ActionResult Index()
        {
            SetupDepartmentViewModel model = new SetupDepartmentViewModel();
            model = _setupDepartmentProvider.GetDepartmentList();
            return View(model);
        }
        public ActionResult Create()
        {
            SetupDepartmentViewModel model = new SetupDepartmentViewModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(SetupDepartmentViewModel model)
        {
            int conditionApply = 1;
            if (!ModelState.IsValid)
            {
                TempData["validationError"] = Utilities.ValidationMessage.validationError;
                return RedirectToAction("Index");
            }
            else
            {
                conditionApply = _setupDepartmentProvider.Save(model);

                if (conditionApply == 2)
                {
                    TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
                }
                else if (conditionApply == 0)
                {
                    TempData["savefailed"] = Utilities.ValidationMessage.savefailed;
                }
                else if (conditionApply == 1)
                {
                    TempData["save"] = Utilities.ValidationMessage.save;
                }
            }

            return RedirectToAction("Index");
        }
        public ActionResult Edit(int? id)
        {
            SetupDepartmentViewModel model = new SetupDepartmentViewModel();
            if (id > 0)
            {
                model = _setupDepartmentProvider.Edit(id ?? 0);
            }

            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(SetupDepartmentViewModel model)
        {
            int condiontApply = 0;
            if (ModelState.IsValid)
            {
                condiontApply = _setupDepartmentProvider.Save(model);
            }
            if (condiontApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.edit;
            }
            else if (condiontApply == 2)
            {
                TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.editfailed;
            }

            return RedirectToAction("Index");
        }
        public ActionResult Delete(int? id)
        {
            int condiontApply = 0;
            condiontApply = _setupDepartmentProvider.Delete(id ?? 0);
            if (condiontApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.delete;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.deletefailed;
            }

            return RedirectToAction("Index");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMS.Models.GuardianInfoViewModel;
using SMS.Services.Providers;
using SMS.Utilities;

namespace SMS.Controllers
{
    [CustomAuthorize(Roles = "SuperAdmin,Admin")]
    public class GuardianInfoController : Controller
    {
        // GET: GuardianInfo
       
        private readonly IGuardianInfoProvider _pro;
        public GuardianInfoController()
        {
            this._pro = new GuardianInfoProvider();
        }

        public ActionResult Index()
        {
            GuardianInfoViewModel model = new GuardianInfoViewModel();
            model = _pro.GetGuardianList();
            return View(model);
        }

        public ActionResult Create()
        {
            GuardianInfoViewModel model = new GuardianInfoViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(GuardianInfoViewModel model)
        {
             
            try
            {
                if (ModelState.IsValid)
                {
                    _pro.Save(model);
                }
            }
            catch (Exception ee)
            {

                throw;
            }

          
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int? id)
        {
            GuardianInfoViewModel model = new GuardianInfoViewModel();
            if (id >0)
            {
                model = _pro.Edit(id ?? 0);
            }
            
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(GuardianInfoViewModel model)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    _pro.Save(model);
                }
            }
            catch (Exception eee)
            {

                throw;
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int? id)
        {
            if (id>0)
            {
                _pro.Delete(id??0);
            }
           
            return RedirectToAction("Index");
        }
    }
}
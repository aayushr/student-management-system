﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMS.Models.StaffInfoViewModel;
using SMS.Service.Providers.StaffInfoProvider;
using SMS.Utilities;

namespace SMS.Controllers
{
    [CustomAuthorize(Roles = "SuperAdmin,Admin")]
    public class StaffInfoController : Controller
    {
        private readonly IStaffInfoProvider _staffInfoProvider;
        public StaffInfoController()
        {
            this._staffInfoProvider = new StaffInfoProvider();
        }
        public ActionResult Index()
        {
            StaffInfoViewModel model = new StaffInfoViewModel();
            model = _staffInfoProvider.GetStaffList();
            return View(model);
        }
        public ActionResult Create()
        {
            StaffInfoViewModel model = new StaffInfoViewModel();
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(StaffInfoViewModel model)
        {
            int conditionApply = 1;
            if (!ModelState.IsValid)
            {
                TempData["validationError"] = Utilities.ValidationMessage.validationError;
                return RedirectToAction("Index");

            }
            else
            {
                conditionApply = _staffInfoProvider.Save(model);
                if (conditionApply == 2)
                {
                    TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
                }
                else if (conditionApply == 0)
                {
                    TempData["savefailed"] = Utilities.ValidationMessage.savefailed;
                }
                else if (conditionApply == 1)
                {
                    TempData["save"] = Utilities.ValidationMessage.save;
                }

            }
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int? id)
        {
            StaffInfoViewModel model = new StaffInfoViewModel();
            if (id > 0)
            {
                model = _staffInfoProvider.Edit(id ?? 0);
            }
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(StaffInfoViewModel model)
        {
            int condiontApply = 0;
            if (ModelState.IsValid)
            {
                condiontApply = _staffInfoProvider.Save(model);
            }
            if (condiontApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.edit;
            }
            else if (condiontApply == 2)
            {
                TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.editfailed;
            }

            return RedirectToAction("Index");

        }
        public ActionResult Delete(int? id)
        {
            int condiontApply = 0;
            condiontApply = _staffInfoProvider.Delete(id ?? 0);
            if (condiontApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.delete;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.deletefailed;
            }

            return RedirectToAction("Index");
        }
    }
}
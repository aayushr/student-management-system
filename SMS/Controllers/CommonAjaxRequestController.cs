﻿using SMS.Models.CommonViewModel;
using SMS.Models.ReportModelViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMS.SMS.Entities;

namespace SMS.Controllers
{
    public class CommonAjaxRequestController : Controller
    {
        SMSEntities ent = new SMSEntities();

        // GET: CommonAjaxRequest
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult DeleteConformation(int? id,string name,string actionName,string controllerName,string partialActionName,string appendHtmlId)
        {
            DeleteConformationViewModel model = new DeleteConformationViewModel();
            model.ActionName = actionName;
            model.Id = id??0;
            model.ControllerName = controllerName;
            model.Name = name;
            return View(model);
        }
        //ActionResult GetStudentDetailsByStudentId(int id)
        //{

        //    StudentDetailsViewModel model = new StudentDetailsViewModel();

        //    var data = (from s in _ent.StudentInfoes
        //                join ci in _ent.StudentClassInfoes on s.StudentId equals ci.StudentId
        //                join c in _ent.SetupClasses on ci.ClassId equals c.ClassId
        //                join ce in _ent.SetupSections on ci.SectionId equals ce.SectionId
        //                where s.StudentId == id && s.DeletedDate == null
        //                select new
        //                {
        //                    StudentId = s.StudentId,
        //                    StudentCode = s.StudentCode,
        //                    ClassName = c.ClassName,
        //                    SectionName = ce.SectionName,
        //                    FullName = s.FirstName + " " + s.MidName + " " + s.LastName,

        //                }).FirstOrDefault();

        //    if (data != null)
        //    {
        //        model.FullName = data.FullName;
        //        model.ClassName = data.ClassName;
        //        model.SectionName = data.SectionName;
        //        model.StudentCode = data.StudentCode;
        //    }

        //    return Json(model, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult GetFaculties(int id)
        {
            List<SelectListItem> facultyList = new List<SelectListItem>();
            //facultyList.Add(new SelectListItem { Text = "Select Faculty", Value = "0" });
            var FacultyCollection = (from f in ent.SetupFaculties
                                     where f.ClassId == id
                                     select new { f.FacultyId, f.FacultyName }).ToList();
            var SectionCollection = (from s in ent.SetupSections
                                     where s.ClassId == id
                                     select new { s.SectionId, s.SectionName }).ToList();

            foreach (var item in FacultyCollection)
            {
                facultyList.Add(new SelectListItem { Text = item.FacultyName.ToString(), Value = item.FacultyId.ToString() });
            }

           

            return Json(facultyList, JsonRequestBehavior.AllowGet);



        }

        public ActionResult GetSections(int id)
        {
            List<SelectListItem> sectionList = new List<SelectListItem>();
            //sectionList.Add(new SelectListItem { Text = "Select Section", Value = "0" });
            var SectionCollection = (from s in ent.SetupSections
                                     where s.ClassId == id
                                     select new { s.SectionId, s.SectionName }).ToList();

            foreach (var item in SectionCollection)
            {
                sectionList.Add(new SelectListItem { Text = item.SectionName.ToString(), Value = item.SectionId.ToString() });
            }

            return Json(sectionList, JsonRequestBehavior.AllowGet);

        }

    }

    //public ActionResult GetStudentDetailsByStudentId(int id)
    //{
    //    var _ent = new SMSEntities();
    //    StudentReportViewModel model = new StudentReportViewModel();

    //    var data = (from s in _ent.StudentInfoes
    //                join ci in _ent.StudentClassInfoes on s.StudentId equals ci.StudentId
    //                join c in _ent.SetupClasses on ci.ClassId equals c.ClassId
    //                join se in _ent.SetupSections on ci.SectionId equals se.SectionId
    //                join f in _ent.SetupFaculties on ci.FacultyId equals f.FacultyId
    //                where s.StudentId == id && s.DeletedDate == null
    //                select new
    //                {
    //                    StudentId = s.StudentId,
    //                    Level = c.ClassName,
    //                    Section = se.SectionName,
    //                    RollNo = ci.RollNo,
    //                    Course = f.FacultyName,
    //                    StudentName = s.FirstName + " " + s.MidName + " " + s.LastName,

    //                }).FirstOrDefault();

    //    if (data != null)
    //    {
    //        model.StudentName = data.StudentName;
    //        model.RollNo = data.RollNo;
    //        model.Course = data.Course;
    //        //model.Shift = data.;
    //        model.Level = data.Level;
    //        model.Section = data.Section;
    //        // model.ExamType = data.;
    //        //model.AcademicYear = data.;
    //    }

    //    return Json(model, JsonRequestBehavior.AllowGet);

    //}

    

}
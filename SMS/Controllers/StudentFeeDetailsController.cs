﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMS.Service.Providers.AccountSectionProviders;
using SMS.Models.AccountSectionViewModel;
using SMS.Models.SetupViewModel;

namespace SMS.Controllers
{
    public class StudentFeeDetailsController : Controller
    {
        // GET: StudentFeeDetails
        public readonly IStudentFeeDetailsProvider _pro;
        public StudentFeeDetailsController()
        {
            this._pro = new StudentFeeDetailsProvider();
        }
        public ActionResult Index()
        {
            StudentFeeDetailsViewModel model = new StudentFeeDetailsViewModel();
            //model=_pro.GetList();
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(StudentFeeDetailsViewModel model)
        {
            if (ModelState.IsValid)
            {
                _pro.Save(model);
            }
            return RedirectToAction("Index");

        }

        public ActionResult Edit(int? id)
        {
            StudentFeeDetailsViewModel model = new StudentFeeDetailsViewModel();
            model = _pro.Edit(id ?? 0);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(StudentFeeDetailsViewModel model)
        {
            if (ModelState.IsValid)
            {
                _pro.Save(model);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int? id)
        {
            _pro.Delete(id ?? 0);
            return RedirectToAction("Index");
        }



        public ActionResult StudentPaymentDetails()
        {
            StudentFeeDetailsViewModel model = new StudentFeeDetailsViewModel();
            return View();
        }

        public ActionResult ClassPayment()
        {
            StudentFeeDetailsViewModel model = new StudentFeeDetailsViewModel();
            return View(model);
        }

        [HttpPost]

        public ActionResult ClassPayment(StudentFeeDetailsViewModel model)
        {
            model.PaymentForLibraryId = null;
            model.PaymentForHostelId = null;
            model.PaymentForActivitiesId = null;
            //if (ModelState.IsValid)
            //{
            if (model.StudentId != 0 && model.StudentId != null)
            {
                _pro.StudentPaymentDetails(model);
            }

            //}

            return RedirectToAction("Index", "StudentFeeDetails");


        }

        public ActionResult LibraryPayment()
        {
            return View();
        }

        [HttpPost]

        public ActionResult LibraryPayment(StudentFeeDetailsViewModel model)
        {
            if (ModelState.IsValid)
            {
                _pro.StudentPaymentDetails(model);
            }

            return RedirectToAction("Index", "StudentFeeDetails");
        }
        public ActionResult HostelPayment()
        {
            return View();
        }

        [HttpPost]

        public ActionResult HostelPayment(StudentFeeDetailsViewModel model)
        {
            if (ModelState.IsValid)
            {
                _pro.StudentPaymentDetails(model);
            }

            return RedirectToAction("Index", "StudentFeeDetails");
        }

    }
}
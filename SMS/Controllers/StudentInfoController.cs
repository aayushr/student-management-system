﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMS.Models.StudentInfoViewModel;
using SMS.Service.Providers.StudentInfoProvider;
using System.IO;
using SMS.Utilities;

namespace SMS.Controllers
{
    [CustomAuthorize(Roles = "SuperAdmin,Admin")]
    public class StudentInfoController : Controller
    {
        private readonly IStudentInfoProvider _pro;
        
        
        public StudentInfoController()
        {
            this._pro = new StudentInfoProvider();
        }
        public ActionResult Index()
        {
            StudentInfoViewModel model = new StudentInfoViewModel();
            model = _pro.GetList();
            return View(model);
        }
        public ActionResult Create()
        {
            StudentInfoViewModel model = new StudentInfoViewModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(StudentInfoViewModel model,HttpPostedFileBase file)
        {
            //         if (ModelState.IsValid)
            //            {
            //                var imagePath = "~/Content/images/";
            //                if (file != null)
            //                {
            //                    var imageName = Path.GetFileName(file.FileName);
            //                    var path = Path.Combine(Server.MapPath(imagePath), imageName);
            //                    file.SaveAs(path);
            //                    model.ImageName = imageName;
            //                    model.ImagePath = imagePath + file.FileName;
            //                }
            //                _pro.Save(model);

            ////                var errors = ModelState
            ////.Where(x => x.Value.Errors.Count > 0)
            ////.Select(x => new { x.Key, x.Value.Errors })
            ////.ToArray();
            //            }
            //            return RedirectToAction("Index");


            int conditionApply = 1;
            if (!ModelState.IsValid)
            {
                TempData["validationError"] = Utilities.ValidationMessage.validationError;
                return RedirectToAction("Index");
            }
            else
            {
                conditionApply = _pro.Save(model);

                if (conditionApply == 2)
                {
                    TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
                }
                else if (conditionApply == 0)
                {
                    TempData["savefailed"] = Utilities.ValidationMessage.savefailed;
                }
                else if (conditionApply == 1)
                {
                    TempData["save"] = Utilities.ValidationMessage.save;
                }
            }

            return RedirectToAction("Index");

        }

        public ActionResult Edit(int? id)
        {
            StudentInfoViewModel model = new StudentInfoViewModel();
            model = _pro.Edit(id??0);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(StudentInfoViewModel model, HttpPostedFileBase file)
        {
            //if (ModelState.IsValid)
            //{
            //    var imagePath = "~/Content/images/";
            //    if (file != null)
            //    {
            //        var imageName = Path.GetFileName(file.FileName);
            //        var path = Path.Combine(Server.MapPath(imagePath), imageName);
            //        file.SaveAs(path);
            //        model.ImageName = imageName;
            //        model.ImagePath = imagePath + file.FileName;
            //    }
            //    _pro.Save(model);
            //}
            //return RedirectToAction("Index");

            int condiontApply = 0;
            if (ModelState.IsValid)
            {
                condiontApply = _pro.Save(model);
            }
            if (condiontApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.edit;
            }
            else if (condiontApply == 2)
            {
                TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.editfailed;
            }

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int? id)
        {
            //_pro.Delete(id??0);
            // return RedirectToAction("Index");

            int condiontApply = 0;
            condiontApply = _pro.Delete(id ?? 0);
            if (condiontApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.delete;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.deletefailed;
            }

            return RedirectToAction("Index");
        }

       
        public ActionResult StudentRegister()
        {
            StudentInfoViewModel model = new StudentInfoViewModel();
            return View();
        }

        public ActionResult InitStudentCreate()
        {
            StudentInfoViewModel model = new StudentInfoViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult InitStudentCreate(StudentInfoViewModel model)
        {
            if (ModelState.IsValid)
            {
                _pro.InitRegistration(model);
            }

            var errors = ModelState
    .Where(x => x.Value.Errors.Count > 0)
    .Select(x => new { x.Key, x.Value.Errors })
    .ToArray();
            return RedirectToAction("Index", "StudentInfo");


        }

        //public ActionResult InitParentCreate()
        //{
        //    StudentInfoViewModel model = new StudentInfoViewModel();
        //    return View(model);
        //}

        //public ActionResult InitGuardianCreate()
        //{
        //    StudentInfoViewModel model = new StudentInfoViewModel();
        //    return View(model);
        //}

       

    }
}
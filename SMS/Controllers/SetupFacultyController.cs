﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SMS.Service.Providers.SetupFacultyProvider;
using SMS.Models.SetupViewModel;
using SMS.Utilities;

namespace SMS.Controllers
{
    //[CustomAuthorize(Roles = "SuperAdmin,Admin")]
    public class SetupFacultyController : Controller
    {
        // GET: SetupFaculty
        private readonly SetupFacultyProvider _pro;
        public SetupFacultyController()
        {
            this._pro = new SetupFacultyProvider();
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult _GetFacultyList(string name)
        {
            SetupFacultyViewModel model = new SetupFacultyViewModel();
            model = _pro.GetList(name);
            return View(model);
        }
        public ActionResult Create()
        {
            SetupFacultyViewModel model = new SetupFacultyViewModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(SetupFacultyViewModel model)
        {

            int conditionApply = 1;
            if (!ModelState.IsValid)
            {
                TempData["validationError"] = Utilities.ValidationMessage.validationError;

                return Json(Utilities.ValidationMessage.validationError, JsonRequestBehavior.AllowGet);
                // return RedirectToAction("Index");
            }
            else
            {
                conditionApply = _pro.Save(model);

                if (conditionApply == 2)
                {
                    //TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
                    return Json(Utilities.ValidationMessage.AlreadyExits, JsonRequestBehavior.AllowGet);

                }

                else if (conditionApply == 1)
                {
                    //TempData["save"] = Utilities.ValidationMessage.save;
                    return Json(Utilities.ValidationMessage.save, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    //TempData["savefailed"] = Utilities.ValidationMessage.savefailed;
                    return Json(Utilities.ValidationMessage.savefailed, JsonRequestBehavior.AllowGet);
                }
            }
            // return View(model);

            //return Json(data, JsonRequestBehavior.AllowGet);

        }
        public ActionResult Edit(int? id)
        {
            SetupFacultyViewModel model = new SetupFacultyViewModel();
            model = _pro.Edit(id ?? 0);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(SetupFacultyViewModel model)
        {
            int condiontApply = 0;
            if (ModelState.IsValid)
            {
                condiontApply = _pro.Save(model);
            }
            if (condiontApply == 1)
            {
                // TempData["save"] = Utilities.ValidationMessage.edit;
                return Json(Utilities.ValidationMessage.edit, JsonRequestBehavior.AllowGet);
            }
            else if (condiontApply == 2)
            {
                //TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
                return Json(Utilities.ValidationMessage.AlreadyExits, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //TempData["savefailed"] = Utilities.ValidationMessage.editfailed;
                return Json(Utilities.ValidationMessage.editfailed, JsonRequestBehavior.AllowGet);
            }

            // return RedirectToAction("Index");
        }
        public JsonResult Delete(int? id)
        {
            int condiontApply = 0;
            condiontApply = _pro.Delete(id ?? 0);
            if (condiontApply == 1)
            {
                //TempData["save"] = Utilities.ValidationMessage.delete;
                return Json(Utilities.ValidationMessage.delete, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //TempData["savefailed"] = Utilities.ValidationMessage.deletefailed;
                return Json(Utilities.ValidationMessage.deletefailed, JsonRequestBehavior.AllowGet);
            }

        }
    }
}
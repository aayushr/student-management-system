﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SMS.Service.Providers.ParentInfoProviders;
using SMS.Models.ParentInfoViewModel;
using System.Data.Entity.Validation;
using SMS.Utilities;

namespace SMS.Controllers
{
    [CustomAuthorize(Roles = "SuperAdmin,Admin")]
    public class ParentInfoController : Controller
    {
        private readonly IParentInfoProvider _pro;
        public ParentInfoController()
        {
            this._pro = new ParentInfoProvider();
        }
        public ActionResult Index()
        {
            ParentInfoViewModel model = new ParentInfoViewModel();
            model = _pro.GetParentList();
            return View(model);
        }
        public ActionResult Create()
        {
            ParentInfoViewModel model = new ParentInfoViewModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(ParentInfoViewModel model)
        {
            int conditionApply = 1;
            if (!ModelState.IsValid)
            {
                TempData["validationError"] = Utilities.ValidationMessage.validationError;
                return RedirectToAction("Index");
            }
            conditionApply = conditionApply = _pro.Save(model);
            if (conditionApply == 0)
            {
                TempData["savefailed"] = Utilities.ValidationMessage.savefailed;
            }
            else if (conditionApply == 2)
            {
                TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
            }
            else
            {
                TempData["save"] = Utilities.ValidationMessage.save;

            }
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int? id)
        {
            ParentInfoViewModel model = new ParentInfoViewModel();
            model = _pro.Edit(id ?? 0);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(ParentInfoViewModel model)
        {
            int conditionApply = 0;
            if (ModelState.IsValid)
            {
                conditionApply = _pro.Save(model);
            }
            if (conditionApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.edit;
            }
            else if (conditionApply == 2)
            {
                TempData["AlreadyExits"] = Utilities.ValidationMessage.AlreadyExits;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.editfailed;
            }



            return RedirectToAction("Index");
        }
        public ActionResult Delete(int? id)
        {
            int condiontApply = 0;
            condiontApply = _pro.Delete(id??0);
            
            if (condiontApply == 1)
            {
                TempData["save"] = Utilities.ValidationMessage.delete;
            }
            else
            {
                TempData["savefailed"] = Utilities.ValidationMessage.deletefailed;
            }

            return RedirectToAction("Index");
        }
    }
}

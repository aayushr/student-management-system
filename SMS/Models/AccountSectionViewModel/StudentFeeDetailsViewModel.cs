﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMS.Models.AccountSectionViewModel
{
    public class StudentFeeDetailsViewModel
    {
        public int StudentFeeDetailsId { get; set; }
        public int StudentId { get; set; }
        public int? ClassId { get; set; }
        public int? FacultyId { get; set; }
        public int? PaymentForClassId { get; set; }
        public int? PaymentForLibraryId { get; set; }
        public int? PaymentForHostelId { get; set; }
        public int? PaymentForActivitiesId { get; set; }
        public string PaidBy { get; set; }
        public decimal Payment { get; set; }
        public string Relation { get; set; }
        public int ReceivedBy { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public bool Status { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }


        public string StudentName { get; set; }
        public string Class { get; set; }
        public string Faculty { get; set; }
        public string Library { get; set; }
        public string Hostel { get; set; }
        public string Activities { get; set; }


        public List<StudentFeeDetailsViewModel> StudentFeeDetailsViewModelList { get; set; }




        public SetupViewModel.SetupPaymentForClassViewModel ObjSetupPaymentForClassViewModel { get; set; }
        public SetupViewModel.SetupPaymentForLibraryViewModel ObjSetupPaymentForLibraryViewModel { get; set; }
        public SetupViewModel.SetupPaymentForHostelViewModel ObjSetupPaymentForHostelViewModel { get; set; }
        public StudentInfoViewModel.StudentInfoViewModel ObjStudentInfoViewModel { get; set; }



        public StudentFeeDetailsViewModel()
        {
            ObjSetupPaymentForClassViewModel = new SetupViewModel.SetupPaymentForClassViewModel();
            ObjSetupPaymentForLibraryViewModel = new SetupViewModel.SetupPaymentForLibraryViewModel();
            ObjSetupPaymentForHostelViewModel = new SetupViewModel.SetupPaymentForHostelViewModel();
            ObjStudentInfoViewModel = new StudentInfoViewModel.StudentInfoViewModel();
        }

    }
}
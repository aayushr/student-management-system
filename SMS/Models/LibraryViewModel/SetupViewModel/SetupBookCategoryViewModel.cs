﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMS.Models.SetupViewModel
{
    public class SetupBookCategoryViewModel
    {
        public int BookCategoryId { get; set; }
        public string CategoryName { get; set; }
        public bool Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime DeletedDate { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public int DeletedBy { get; set; }
        public List<SetupBookCategoryViewModel> SetupBookCategoryViewModelList { get; set; }
    }
}
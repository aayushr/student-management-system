﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMS.Models.LibraryViewModel
{
    public class SetupBookViewModel
    {
        public int BookId { get; set; }
        public string CategoryName { get; set; }
        public int? BookCategoryId { get; set; }
        public string BookName { get; set; }
        public string AuthorName { get; set; }
        public string Publisher { get; set; }
        public int? Volume { get; set; }
        public string Edition { get; set; }
        public bool Status { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public DateTime? PublishedDate { get; set; }
        public List<SetupBookViewModel> SetupBookViewModelList { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMS.Models.SetupViewModel
{
    public class SetupSectionViewModel
    {

        public int SectionId { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        [Required(ErrorMessage = "Section Name is Required")]
        [Display(Name = "Section Name")]
        public string SectionName { get; set; }
        public bool Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public int? DeletedBy { get; set; }

        public List<SetupSectionViewModel> SetupSectionViewModelList { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMS.Models.SetupViewModel
{
    public class SetupTerminalExamViewModel
    {
        public int TerminalExamId { get; set; }
        public string Name { get; set; }
        public string Remarks { get; set; }
        public bool Status { get; set; }
        public int? CreatedBY { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public List<SetupTerminalExamViewModel> SetupTerminalExamViewModelList { get; set; }
    }
}
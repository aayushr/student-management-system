﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMS.Models.SetupViewModel
{
    public class SetupPaymentForLibraryViewModel
    {
        public int PaymentForLibraryId { get; set; }
        public string LibraryName { get; set; }
        public int? ClassId { get; set; }
        public string ClassName { get; set; }
        public string FacultyName { get; set; }
        public int? FacultyId { get; set; }
        public decimal LibraryFee { get; set; }
        public bool Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public int? DeletedBy { get; set; }

       public List<SetupPaymentForLibraryViewModel> SetupPaymentForLibraryViewModelList { get; set; }
        public List<SetupViewModel.SetupClassViewModel> SetupClassViewModelList { get; set; }
        public SetupPaymentForLibraryViewModel()
        {
            SetupClassViewModelList = new List<SetupClassViewModel>();
        }


    }
}
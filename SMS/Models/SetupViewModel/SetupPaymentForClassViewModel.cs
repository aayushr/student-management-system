﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMS.Models.SetupViewModel
{
    public class SetupPaymentForClassViewModel
    {
        public int PaymentForClassId { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        [Required(ErrorMessage = "Class Fee is Required")]
        [Display(Name = "Class Fee")]
        public decimal? ClassFee { get; set; }
        public bool Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public int? DeletedBy { get; set; }



        public List<SetupPaymentForClassViewModel> SetupPaymentForClassViewModelList { get; set; }
    }
}
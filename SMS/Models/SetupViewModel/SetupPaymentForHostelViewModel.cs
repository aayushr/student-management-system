﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMS.Models.SetupViewModel
{
    public class SetupPaymentForHostelViewModel
    {
        public int PaymentForHostelId { get; set; }
        public string ClassName { get; set; }
        public string FacultyName { get; set; }
        public string HostelName { get; set; }
        public int? ClassId { get; set; }
        public int? FacultyId { get; set; }
        public decimal HostelFee { get; set; }
        public bool Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public int? DeletedBy { get; set; }
        public List<SetupPaymentForHostelViewModel> SetupPaymentForHostelViewModelList { get; set; }


    }
}
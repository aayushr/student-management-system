﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMS.Models.SetupViewModel
{
    public class SetupPaymentForActivityViewModel
    {

        public int PaymentForActivitiesId { get; set; }
        [Required(ErrorMessage="Activity Name is Required")]
        [Display(Name ="Activity Name")]
        public string ActivitiesName { get; set; }
        public string ClassName { get; set; }
        public int? ClassId { get; set; }
        public string FacultyName { get; set; }
        public int? FacultyId { get; set; }
        public bool Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public int? DeletedBy { get; set; }

        public List<SetupPaymentForActivityViewModel> SetupPaymentForActivityViewModelList { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMS.Models.SetupViewModel
{
    public class SetupServiceViewModel
    {
        public int ServiceId { get; set; }
        [Required(ErrorMessage ="Service Name is required")]
        [Display(Name ="Service Name")]
        public string ServiceName { get; set; }
        public int? ServiceOrder { get; set; }
        public bool Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public int? DeletedBy { get; set; }
        public List<SetupServiceViewModel> SetupServiceViewModelList { get; set; }

    }
}
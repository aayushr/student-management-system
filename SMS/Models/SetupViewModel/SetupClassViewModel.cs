﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMS.Models.SetupViewModel
{
    public class SetupClassViewModel
    {
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public bool Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public int? DeletedBy { get; set; }
        public List<SetupClassViewModel> SetupClassViewModelList { get; set; }
        public List<SetupSubjectViewModel> SetupSubjectViewModelList { get; set; }
        public SetupClassViewModel()
        {
            SetupSubjectViewModelList = new List<SetupSubjectViewModel>();
            SetupClassViewModelList = new List<SetupClassViewModel>();

        }

    }
    public class ClassSubjectViewModel
    {
        public int ClassSubjectId { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public string SubjectName { get; set; }
        public int SubjectId { get; set; }
        public bool Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public int? DeletedBy { get; set; }
        public List<ClassSubjectViewModel> ClassSubjectViewModelList { get; set; }
        public List<SetupSubjectViewModel> SetupSubjectViewModelList { get; set; }
        public ClassSubjectViewModel()
        {
            SetupSubjectViewModelList = new List<SetupSubjectViewModel>();
        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMS.Models.SetupViewModel
{
    public class SetupPostViewModel
    {
        public int PostId { get; set; }
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public bool Status { get; set; }
        [Required(ErrorMessage = "Post Name is Required")]
        [Display(Name = "Post Name")]
        public string PostName { get; set; }
        [Required(ErrorMessage = "Post Order is Required")]
        [Display(Name = "Post Order")]
        public int? PostOrder { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public int? DeletedBy { get; set; }

        public List<SetupPostViewModel> SetupPostViewModelList { get; set; }
    }
}
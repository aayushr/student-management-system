﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMS.Models.StaffInfoViewModel
{
    public class StaffInfoViewModel
    {
        public int StaffId { get; set; }
        [Required(ErrorMessage = "Service Name is Required")]
        [Display(Name = "Service Name")]
        public int ServiceId { get; set; }
        public int UserId { get; set; }
        [Required(ErrorMessage = "Post Name is Required")]
        [Display(Name = "Post Name")]
        public int PostId { get; set; }
        public int DepartmentId { get; set; }
        [Required(ErrorMessage = "First Name is Required")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Middle Name")]
        public string MidName { get; set; }
        [Required(ErrorMessage = "Last Name is Required")]
        [Display(Name = "First Name")]
        public string LastName { get; set; }
        [Display(Name = "Permanent State")]
        public string PState { get; set; }
        [Display(Name = "Permanent District")]
        public string PDistrict { get; set; }
        [Display(Name = "Permanent Metropolitan")]
        public string PMetropolitan { get; set; }
        [Display(Name = "Permanent SubMetropolitan")]
        public string PSubMetropolitan { get; set; }
        [Display(Name = "Permanent Municipality")]
        public string PMunicipality { get; set; }
        [Display(Name = "Permanent GauPalika")]
        public string PGauPalika { get; set; }
        [Display(Name = "Permanent Ward No.")]
        public string PWardNo { get; set; }
        [Display(Name = "Temporary State")]
        public string TState { get; set; }
        [Display(Name = "Temporary District")]
        public string TDistrict { get; set; }
        [Display(Name = "Temporary Metropolitan")]
        public string TMetropolitan { get; set; }
        [Display(Name = "Temporary SubMetropolitan")]
        public string TSubMetropolitan { get; set; }
        [Display(Name = "Temporary Municipality")]
        public string TMunicipality { get; set; }
        [Display(Name = "Temporary GauPalika")]
        public string TGauPalika { get; set; }
        [Display(Name = "Temporary WardNo.")]
        public string TWardNo { get; set; }
        [Display(Name = "BloodGroup")]
        public string BloodGroup { get; set; }
        [Display(Name = "Occupation")]
        public string Occupation { get; set; }
        [Display(Name = "Gender")]
        public string Gender { get; set; }
        [Display(Name = "EmailId")]
        public string EmailId { get; set; }
        [Display(Name = "MobilelNo.")]
        public string CellNumber { get; set; }
        [Display(Name = "ResidanceNo.")]
        public long? PhoneNumber { get; set; }
        [Display(Name = "CitizenshipNo.")]
        public string CitizenNumber { get; set; }
        [Display(Name = "Status")]
        public bool Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public int? DeletedBy { get; set; }
        public List<StaffInfoViewModel> StaffInfoViewModelList { get; set; }


    }
}
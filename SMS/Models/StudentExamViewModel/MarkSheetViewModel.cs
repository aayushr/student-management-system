﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMS.Models.MarkSheetViewModel
{
    public class MarkSheetViewModel
    {
        public int MarkSheetId { get; set; }
        public int? TerminalExamId { get; set; }
        public string TerminalExamName { get; set; }
        public int? StudentId { get; set; }
        public string StudentName { get; set; }
        public string ClassName { get; set; }
        
        public int? ClassId { get; set; }
        public string FacultyName { get; set; }
        public int? FacultyId { get; set; }
        public string SectionName { get; set; }
        public int? SectionId { get; set; }
        public string SubjectName { get; set; }
        public int? SubjectId { get; set; }
        public decimal? ObtainThroryMarks { get; set; }
        public decimal? ObtainPracticalMarks { get; set; }
        public decimal? ObtainTotalMarks { get; set; }
        public string Remarks { get; set; }
        public bool Status { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public List<MarkSheetViewModel> MarkSheetViewModelList { get; set; }

    }
}
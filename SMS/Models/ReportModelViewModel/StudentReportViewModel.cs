﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMS.Models.ReportModelViewModel
{
    public class StudentReportViewModel
    {

        public string StudentName { get; set; }
        public string RollNo { get; set; }
        public string Course { get; set; }
        public string Shift { get; set; }
        public string Level { get; set; }
        public string Section { get; set; }
        public string ExamType { get; set; }
        public string AcademicYear { get; set; }
        public int StudentId { get; set; }


        

        public string Subject { get; set; }
        public int FullMarks { get; set; }
        public decimal PassMarks { get; set; }
        public decimal ObtainedMarks { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BloodGroup { get; set; }



        public List<StudentReportViewModel> StudentReportViewModelList { get; set; }


        public StudentInfoViewModel.StudentInfoViewModel ObjStudentInfoViewModel { get; set; }
        public SetupViewModel.SetupFacultyViewModel ObjSetupFacultyViewModel { get; set; }
        public SetupViewModel.SetupClassViewModel ObjSetupClassViewModel { get; set; }
        public SetupViewModel.ClassSubjectViewModel ObjClassSubjectViewModel { get; set; }
        public SetupViewModel.SetupSectionViewModel ObjSetupSectionViewModel { get; set; }
        public SetupViewModel.SetupSubjectViewModel ObjSetupSubjectViewModel { get; set; }
        public StudentInfoViewModel.StudentSubjectDetailsViewModel ObjStudentSubjectDetailsViewModel { get; set; }
        
        
        


        public StudentReportViewModel()
        {
            ObjStudentInfoViewModel = new StudentInfoViewModel.StudentInfoViewModel();
            ObjSetupFacultyViewModel = new SetupViewModel.SetupFacultyViewModel();
            ObjSetupClassViewModel = new SetupViewModel.SetupClassViewModel();
            ObjClassSubjectViewModel = new SetupViewModel.ClassSubjectViewModel();
            ObjSetupSectionViewModel = new SetupViewModel.SetupSectionViewModel();
            ObjSetupSubjectViewModel = new SetupViewModel.SetupSubjectViewModel();
            ObjStudentSubjectDetailsViewModel = new StudentInfoViewModel.StudentSubjectDetailsViewModel();
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SMS.Models.GuardianInfoViewModel
{
    public class GuardianInfoViewModel
    {
        public int GuardianId { get; set; }
        public int StudentId { get; set; }
        [Required(ErrorMessage = "First Name is Required")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Middle Name")]
        public string MidName { get; set; }
        [Required(ErrorMessage = "Last Name is Required")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Permanent State is Required")]
        [Display(Name = "Permanent State")]
        public string PState { get; set; }
        [Required(ErrorMessage = "Permanent District is Required")]
        [Display(Name = "Permanent District")]
        public string PDistrict { get; set; }
        [Display(Name = "Permanent Metropolitan")]
        public string PMetropolitan { get; set; }
        [Display(Name = "Permanent Sub-Metropolitan")]
        public string PSubMetropolitan { get; set; }
        [Display(Name = "Permanent Municipality")]
        public string PMunicipality { get; set; }
        [Display(Name = "Permanent GauPalika")]
        public string PGauPalika { get; set; }
        [Display(Name = "Permanent Ward No")]
        public string PWardNo { get; set; }
        [Display(Name = "Temporary State")]
        public string TState { get; set; }
        [Display(Name = "Temporary District")]
        public string TDistrict { get; set; }
        [Display(Name = "Temporary Metropolitan")]
        public string TMetropolitan { get; set; }
        [Display(Name = "Temporary Sub-Metropolitan")]
        public string TSubMetropolitan { get; set; }
        [Display(Name = "Temporary Municipality")]
        public string TMunicipality { get; set; }
        [Display(Name = "Temporary GauPalika")]
        public string TGauPalika { get; set; }
        [Display(Name = "Temporary WardNo")]
        public string TWardNo { get; set; }
        [Display(Name = "BloodGroup")]
        public string BloodGroup { get; set; }
        [Display(Name = "Occupation")]
        public string Occupation { get; set; }
        [Display(Name = "Gender")]
        public string Gender { get; set; }
        [Display(Name = "EmailId")]
        public string EmailId { get; set; }
        [Required(ErrorMessage = "Mobile Number is Required")]
        [Display(Name = "Mobile Number")]
        public string CellNumber { get; set; }
        public Nullable<long> PhoneNumber { get; set; }
        [Display(Name = "Citizen Number")]
        public string CitizenNumber { get; set; }
        public bool Status { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<System.DateTime> DeletedDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<int> DeletedBy { get; set; }

        [Display(Name = "Full Name")]
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
        public List<GuardianInfoViewModel> GuardianInfoViewModelList { get; set; }
    }
}
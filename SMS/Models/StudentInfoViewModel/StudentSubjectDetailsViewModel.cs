﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace SMS.Models.StudentInfoViewModel
{
    public class StudentSubjectDetailsViewModel
    {
        public int StudentSubjectId { get; set; }
        public string StudentName { get; set; }
        public string SubjectName { get; set; }
        public int? StudentId { get; set; }
        public int? SubjectId { get; set; }
        public bool Status { get; set; }
        public bool IsChecked { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public int? DeletedBy { get; set; }
        public List<StudentSubjectDetailsViewModel> StudentSubjectDetailsViewModelList { get; set; }
        public List<SetupViewModel.SetupSubjectViewModel> SetupSubjectViewModelList { get; set; }
        public StudentSubjectDetailsViewModel()
        {
            SetupSubjectViewModelList = new List<SetupViewModel.SetupSubjectViewModel>();
            StudentSubjectDetailsViewModelList = new List<StudentSubjectDetailsViewModel>();

        }
    }


}
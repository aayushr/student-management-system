﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace SMS.Models.StudentInfoViewModel
{
    public class StudentInfoViewModel
    {
        public int StudentId { get; set; }
        [Required(ErrorMessage = "Student Code is Required")]
        [Display(Name = "Student Code")]
        public string StudentCode { get; set; }
        [Required(ErrorMessage = "First Name is Required")]
        [Display(Name ="First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Middle Name")]
        public string MidName { get; set; }
        [Required(ErrorMessage = "Last Name is Required")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "Student Type")]
        public string StudentType { get; set; }
        [Display(Name = "Religion")]
        public string Religion { get; set; }
        [Display(Name = "IsTransportation")]
        public bool IsTransportation { get; set; }
        public int ClassId { get; set; }
        public int? FacultyId { get; set; }

        public int? SectionId { get; set; }
        public int? BlockId { get; set; }
        [Display(Name = "IsNew")]
        public bool IsNew { get; set; }
        [Display(Name = "Reference")]
        public string Reference { get; set; }
        [Required(ErrorMessage = "Permanent State is Required")]
        [Display(Name = "Permanent State")]
        public string PState { get; set; }
        [Required(ErrorMessage = "Permanent District is Required")]
        [Display(Name = "Permanent District")]
        public string PDistrict { get; set; }
        [Display(Name = "Permanent Metropolitan")]
        public string PMetropolitan { get; set; }
        [Display(Name = "Permanent Sub-Metropolitan")]
        public string PSubMetropolitan { get; set; }
        [Display(Name = "Permanent Municipality")]
        public string PMunicipality { get; set; }
        [Display(Name = "Permanent GauPalika")]
        public string PGauPalika { get; set; }
        [Display(Name = "Permanent WardNo")]
        public string PWardNo { get; set; }
        [Required(ErrorMessage = "Temporary State is Required")]
        [Display(Name = "Temporary State")]
        public string TState { get; set; }
        [Required(ErrorMessage = "Temporary State is Required")]
        [Display(Name = "Temporary District")]
        public string TDistrict { get; set; }
        [Display(Name = "Temporary Metropolitan")]
        public string TMetropolitan { get; set; }
        [Display(Name = "Temporary Sub-Metropolitan")]
        public string TSubMetropolitan { get; set; }
        [Display(Name = "Temporary Municipality")]
        public string TMunicipality { get; set; }
        [Display(Name = "Temporary GauPalika")]
        public string TGauPalika { get; set; }
        [Display(Name = "Temporary WardNo")]
        public string TWardNo { get; set; }
        [Display(Name = "Blood Group")]
        public string BloodGroup { get; set; }
        [Display(Name = "Enrolled Date")]
        public DateTime? EnrolledDate { get; set; }
        [Display(Name = "DateOfBirth")]
        public DateTime? DateOfBirth { get; set; }
        [Required(ErrorMessage = "EmailId is Required")]
        [Display(Name = "EmailId")]
        public string EmailId { get; set; }
        [Required(ErrorMessage = "Mobile Number is Required")]
        [Display(Name ="Mobile Number")]
        public long? CellNumber { get; set; }
        [Required(ErrorMessage = "Gender is Required")]
        [Display(Name = "Gender")]
        public string Gender { get; set; }
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
        public bool Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public int? DeletedBy { get; set; }



        public ParentInfoViewModel.ParentInfoViewModel ObjParentInfoViewModel { get; set; }
        public GuardianInfoViewModel.GuardianInfoViewModel ObjGuardianInfoViewModel { get; set; }
        public StudentInfoViewModel()
        {
            ObjParentInfoViewModel = new ParentInfoViewModel.ParentInfoViewModel();
            ObjGuardianInfoViewModel = new GuardianInfoViewModel.GuardianInfoViewModel();
        }

        public List<StudentInfoViewModel> StudentInfoViewModelList { get; set; }

    }
    public class StudentPaymentReport
    {
        public string StudetnName { get; set; }
        public string Class { get; set; }
        public string Section { get; set; }
        public string PreviousMonthlyFee { get; set; }
        public string ThisMonthlyFee { get; set; }
        public decimal PreviouTutionFee { get; set; }
        public decimal ThisMonthTutionFee { get; set; }
        //public string PreviousMonthlyFee { get; set; }

    }
}
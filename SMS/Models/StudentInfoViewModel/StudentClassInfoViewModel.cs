﻿using SMS.SMS.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SMS.Models.StudentInfoViewModel
{
    public class StudentClassInfoViewModel
    {
        public int StudentClassId { get; set; }
        public string StudentName { get; set; }
        public string ClassName { get; set; }
        public string SectionName { get; set; }
        public string BlockName { get; set; }
        public string FacultyName { get; set; }
        public int StudentId { get; set; }
        public int ClassId { get; set; }
        public int? SectionId { get; set; }
        public int? BlockId { get; set; }
        public int? FacultyId { get; set; }
        [Required(ErrorMessage = "Roll Number is Required")]
        [Display(Name = "Roll Number")]
        public string RollNo { get; set; }
        public bool Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public int? DeletedBy { get; set; }

       

        public List<StudentClassInfoViewModel> StudentClassInfoViewModelList { get; set; }
    }
}
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SMS.SMS.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class SetupPost
    {
        public int PostId { get; set; }
        public int ServiceId { get; set; }
        public Nullable<bool> Status { get; set; }
        public string PostName { get; set; }
        public Nullable<int> PostOrder { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<System.DateTime> DeletedDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<int> DeletedBy { get; set; }
    
        public virtual SetupService SetupService { get; set; }
    }
}

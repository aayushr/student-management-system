﻿using SMS.Models.StaffInfoViewModel;
using SMS.SMS.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;

namespace SMS.Service.Providers.Userprovider
{
    public class CustomeUserManager
    {

        private static int userId;
        public static StaffInfoViewModel GetUserDetails()
        {
            StaffInfoViewModel model = new StaffInfoViewModel();
            var ClaimsIdentity = Thread.CurrentPrincipal.Identity as ClaimsIdentity;
            if (ClaimsIdentity != null)
            {
                var Claim = ClaimsIdentity.FindFirst("UserId");
                if (Claim != null && !String.IsNullOrEmpty(Claim.Value))
                {
                    userId = Convert.ToInt32(Claim.Value);
                }

            }

            using (SMSEntities _ent = new SMSEntities())
            {
                try
                {

                    var data = _ent.StaffInfoes.SingleOrDefault(x => x.StaffId == userId);
                    model.StaffId = userId;
                    model.FirstName = data.FirstName;
                    model.LastName = data.LastName;
                    model.EmailId = data.EmailId;
                    model.DepartmentId = data.DepartmentId;
                    model.ServiceId = data.ServiceId;
                    model.PostId = data.PostId;
                    model.UserId = userId;
                    

                }
                catch (Exception ex)
                {

                    ex.ToString();
                }
                return model;
            }


        }
    }
}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMS.Models.SetupViewModel;
using System.Data.Entity;
using SMS.SMS.Entities;

namespace SMS.Service.Providers.SetupProviders
{
    public interface ISetupBlockProvider
    {
        int Save(SetupBlockViewModel model);
        SetupBlockViewModel GetBlockList();
        SetupBlockViewModel Edit(int blockId);
        int  Delete(int blockId);

    }
    public class SetupBlockProvider : ISetupBlockProvider
    {
        public readonly SMSEntities _ent;
        public SetupBlockProvider()
        {
            this._ent = new SMSEntities();
        }

        public int  Delete(int blockId)
        {

            var user = Userprovider.CustomeUserManager.GetUserDetails();
            var data = _ent.SetupBlocks.Where(x => x.BlockId == blockId).FirstOrDefault();
            if (data != null)
            {
                data.DeletedBy = user.UserId;
                data.DeletedDate = DateTime.UtcNow;
                _ent.Entry(data).State = EntityState.Modified;
                _ent.SaveChanges();
                try
                {
                    return 1;
                }
                catch (Exception ee)
                {

                    return 0;
                }
            }
            return 0;
           
        }

        public SetupBlockViewModel Edit(int blockId)
        {
            SetupBlockViewModel model = new SetupBlockViewModel();
            var data = _ent.SetupBlocks.Where(x => x.BlockId == blockId).FirstOrDefault();
            if (data != null)
            {
                model.BlockId = data.BlockId;
                model.BlockName = data.BlockName;
                model.Status = data.Status??false;
            }
            return model;
        }

        public SetupBlockViewModel GetBlockList()
        {
            SetupBlockViewModel model = new SetupBlockViewModel();
            model.SetupBlockViewModelList = (from s in _ent.SetupBlocks
                                             where s.DeletedDate == null
                                             select new SetupBlockViewModel
                                             {
                                                 BlockId = s.BlockId,
                                                 BlockName = s.BlockName,
                                                 Status = s.Status??false

                                             }).ToList();
            return model;
        }

        public int Save(SetupBlockViewModel model)
        {
            var user = Userprovider.CustomeUserManager.GetUserDetails();
            var data = _ent.SetupBlocks.Where(x => x.BlockName == model.BlockName.Trim() && x.DeletedDate==null).ToList();
            if (data.Count > 0)
            {
                return 2;
            }
            SetupBlock setupBlockEntity = new SetupBlock();
            setupBlockEntity.BlockId = model.BlockId;
            setupBlockEntity.BlockName = model.BlockName.Trim();
            setupBlockEntity.Status = true;
            if (model.BlockId > 0)
            {
                setupBlockEntity.BlockId = model.BlockId;
                setupBlockEntity.UpdatedBy =user.UserId;
                setupBlockEntity.UpdatedDate = DateTime.UtcNow;
                _ent.Entry(setupBlockEntity).State = EntityState.Modified;
                _ent.Entry(setupBlockEntity).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(setupBlockEntity).Property(x => x.CreatedDate).IsModified = false;
            }
            else
            {
                setupBlockEntity.CreatedBy = user.UserId;
                setupBlockEntity.CreatedDate = DateTime.UtcNow;
                _ent.Entry(setupBlockEntity).State = EntityState.Added;

            }
            try
            {
                _ent.SaveChanges();
                return 1;

            }
            catch (Exception ex)
            {

                return 0;
            }

        }
    }
}

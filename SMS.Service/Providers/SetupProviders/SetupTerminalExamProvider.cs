﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMS.SMS.Entities;
using SMS.Models.SetupViewModel;
using System.Data.Entity;

namespace SMS.Service.Providers.SetupProviders
{
    public interface ISetupTerminalExamProvider
    {
        int Save(SetupTerminalExamViewModel model);
        SetupTerminalExamViewModel Edit(int terminalExamId);
        SetupTerminalExamViewModel GetTerminalExamList();

        int Delete(int terminalExamId);
    }
    public class SetupTerminalExamProvider : ISetupTerminalExamProvider
    {
        private readonly SMSEntities _ent;
        public SetupTerminalExamProvider()
        {
            this._ent = new SMSEntities();
        }
        public int Delete(int terminalExamId)
        {
            var data = _ent.SetupTerminalExams.Where(x => x.TerminalExamId == terminalExamId).FirstOrDefault();
            if(data!=null)
            {
                data.DeletedBy = 1;
                data.DeletedDate = DateTime.UtcNow;
                _ent.Entry(data).State = EntityState.Modified;
                _ent.SaveChanges();
                try
                {
                    return 1;
                }
                catch (Exception ee)
                {
                    
                    return 0;
                }
            }
            return 0;
        }

        public SetupTerminalExamViewModel Edit(int terminalExamId)
        {
            SetupTerminalExamViewModel model = new SetupTerminalExamViewModel();
            var data = _ent.SetupTerminalExams.Where(x => x.TerminalExamId == terminalExamId).FirstOrDefault();
            if(data!=null)
            {
                model.TerminalExamId = data.TerminalExamId;
                model.Name = data.Name;
                model.Remarks = data.Remarks;
                model.Status = data.Status??false;
            }
            return model;
           
        }

        public SetupTerminalExamViewModel GetTerminalExamList()
        {
            SetupTerminalExamViewModel model = new SetupTerminalExamViewModel();
            model.SetupTerminalExamViewModelList = (from t in _ent.SetupTerminalExams
                                                    where t.DeletedDate == null
                                                    select new SetupTerminalExamViewModel
                                                    {
                                                        TerminalExamId=t.TerminalExamId,
                                                        Name=t.Name,
                                                        Remarks=t.Remarks,
                                                        Status=t.Status??false
                                                    }).ToList();
            return model;
        }

        public int Save(SetupTerminalExamViewModel model)
        {
            if(model.TerminalExamId==0)
            {
                var data = _ent.SetupTerminalExams.Where(x => x.Name == model.Name.Trim()).ToList();
                if(data.Count > 0)
                {
                    return 2;
                }
            }
            SetupTerminalExam setupTerminalExamEntity = new SetupTerminalExam();
            setupTerminalExamEntity.Name = model.Name;
            setupTerminalExamEntity.Remarks = model.Remarks;
           
            if(model.TerminalExamId > 0)
            {
                setupTerminalExamEntity.Status = model.Status;
                setupTerminalExamEntity.TerminalExamId = model.TerminalExamId;
                setupTerminalExamEntity.UpdatedBy = 1;
                setupTerminalExamEntity.UpdatedDate = DateTime.UtcNow;
                _ent.Entry(setupTerminalExamEntity).State = EntityState.Modified;
                _ent.Entry(setupTerminalExamEntity).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(setupTerminalExamEntity).Property(x => x.CreatedDate).IsModified = false;

            }
            else
            {
                setupTerminalExamEntity.Status = true;
                setupTerminalExamEntity.CreatedBy = 1;
                setupTerminalExamEntity.CreatedDate = DateTime.UtcNow;
                _ent.Entry(setupTerminalExamEntity).State = EntityState.Added;
                
            }
            try
            {
                _ent.SaveChanges();
                return 1;
            }
            catch (Exception e)
            {
                
                return 0;
            }
        }
    }
}

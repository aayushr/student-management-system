﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMS.Models.SetupViewModel;
using System.Data.Entity;
using SMS.SMS.Entities;

namespace SMS.Service.Providers.SetupFacultyProvider
{
    interface ISetupFacultyProvider
    {
        int Save(SetupFacultyViewModel model);
        SetupFacultyViewModel Edit(int facultyId);
        int Delete(int facultyId);
        SetupFacultyViewModel GetList(string facultyName);

    }
    public class SetupFacultyProvider : ISetupFacultyProvider
    {
        private readonly SMSEntities _ent;

        public SetupFacultyProvider()
        {
            this._ent = new SMSEntities();
        }

        public int Delete(int facultyId)
        {
            var user = Userprovider.CustomeUserManager.GetUserDetails();
            var data = _ent.SetupFaculties.Where(x => x.FacultyId == facultyId).FirstOrDefault();
            if (data != null)
            {
                data.DeletedBy = user.UserId;
                data.DeletedDate = DateTime.UtcNow;
                _ent.Entry(data).State = EntityState.Modified;
                _ent.SaveChanges();
            }
            try
            {
                _ent.SaveChanges();
                return 1;
            }
            catch (Exception ee)
            {

                return 0;
            }


        }

        public SetupFacultyViewModel Edit(int id)
        {
            SetupFacultyViewModel model = new SetupFacultyViewModel();
            var data = _ent.SetupFaculties.Where(x => x.FacultyId == id).FirstOrDefault();
            if (data != null)
            {
                model.FacultyId = data.FacultyId;
                model.ClassId = data.ClassId;
                model.FacultyName = data.FacultyName;
                model.Status = data.Status ?? false;
            }

            return model;
        }

        public SetupFacultyViewModel GetList(string facultiName)
        {
            SetupFacultyViewModel model = new SetupFacultyViewModel();
            if ((facultiName != null && facultiName != string.Empty) || facultiName == "")
            {
                model.SetupFacultyViewModelList = (from f in _ent.SetupFaculties
                                                   join c in _ent.SetupClasses on f.ClassId equals c.ClassId
                                                   where f.DeletedDate == null && f.FacultyName.Contains(facultiName)
                                                   select new SetupFacultyViewModel
                                                   {
                                                       FacultyId = f.FacultyId,
                                                       FacultyName = f.FacultyName,
                                                       ClassName = c.ClassName,
                                                       Status = f.Status ?? false,
                                                   }).ToList();
                return model;
            }
            model.SetupFacultyViewModelList = (from f in _ent.SetupFaculties
                                               join c in _ent.SetupClasses on f.ClassId equals c.ClassId
                                               where f.DeletedDate == null
                                               select new SetupFacultyViewModel
                                               {
                                                   FacultyId = f.FacultyId,
                                                   FacultyName = f.FacultyName,
                                                   ClassName = c.ClassName,
                                                   Status = f.Status ?? false,
                                               }).ToList();
            return model;
        }

        public int Save(SetupFacultyViewModel model)
        {

            if (model.FacultyId == 0)
            {
                var data = _ent.SetupFaculties.Where(x => x.FacultyName == model.FacultyName.Trim()  && x.DeletedDate == null && x.ClassId==model.ClassId).ToList();
                if (data.Count > 0)
                {
                    return 2;
                }
            }
            var user = Userprovider.CustomeUserManager.GetUserDetails();
            SetupClassViewModel setupclassmodel = new SetupClassViewModel();
            SetupFaculty entitySetupFaculty = new SetupFaculty();
            entitySetupFaculty.FacultyId = model.FacultyId;
            entitySetupFaculty.ClassId = model.ClassId;
            entitySetupFaculty.FacultyName = model.FacultyName;
            entitySetupFaculty.Status = model.Status;
            if (model.FacultyId > 0)
            {
                entitySetupFaculty.UpdatedBy = user.UserId;
                entitySetupFaculty.UpdatedDate = DateTime.UtcNow;
                _ent.Entry(entitySetupFaculty).State = EntityState.Modified;
                _ent.Entry(entitySetupFaculty).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(entitySetupFaculty).Property(X => X.CreatedDate).IsModified = false;
            }
            else
            {
                entitySetupFaculty.CreatedBy = user.UserId;
                entitySetupFaculty.Status = true;
                entitySetupFaculty.CreatedDate = DateTime.UtcNow;
                _ent.Entry(entitySetupFaculty).State = EntityState.Added;

            }
            try
            {
                _ent.SaveChanges();
                return 1;
            }
            catch (Exception ee)
            {

                return 0;
            }




        }
    }
}

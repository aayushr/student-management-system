﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMS.Models.SetupViewModel;
using SMS.SMS.Entities;
using System.Data.Entity;

namespace SMS.Service.Providers.SetupProviders
{
    public interface ISetupProviders
    {
        int Save(SetupClassViewModel mode);
        SetupClassViewModel Edit(int classId);
        SetupClassViewModel GetSetupClassList();
        int Delete(int classId);

    }
    public class SetupClassProvider : ISetupProviders
    {
        private readonly SMSEntities _ent;
        public SetupClassProvider()
        {
            this._ent = new SMSEntities();
        }

        public int Delete(int classId)
        {
            var user = Userprovider.CustomeUserManager.GetUserDetails();
            var data = _ent.SetupClasses.Where(x => x.ClassId == classId).FirstOrDefault();
            if (data != null)
            {
                data.DeletedBy = user.UserId;
                data.DeletedDate = DateTime.UtcNow;
                _ent.Entry(data).State = EntityState.Modified;
                _ent.SaveChanges();
            }
            try
            {
                return 1;
            }
            catch (Exception)
            {

                return 0;
            }
        }


        public SetupClassViewModel Edit(int classId)
        {
            SetupClassViewModel model = new SetupClassViewModel();
            var data = _ent.SetupClasses.Where(x => x.ClassId == classId).FirstOrDefault();
            if (data != null)
            {
                model.ClassId = data.ClassId;
                model.ClassName = data.ClassName;
                model.Status = data.Status ?? false;

            }

            return model;
        }

        public SetupClassViewModel GetSetupClassList()
        {
            SetupClassViewModel model = new SetupClassViewModel();
            model.SetupClassViewModelList = (from c in _ent.SetupClasses
                                             where c.DeletedDate == null
                                             select new SetupClassViewModel
                                             {
                                                 ClassId = c.ClassId,
                                                 ClassName = c.ClassName,
                                                 Status = c.Status ?? false

                                             }).ToList();
            return model;

        }

        
        public int Save(SetupClassViewModel model)
        {
            var user = Userprovider.CustomeUserManager.GetUserDetails();

            if (model.ClassId > 0)
            {
                SetupClass entitySetupClass = new SetupClass();
                entitySetupClass.ClassId = model.ClassId;
                entitySetupClass.UpdatedBy = user.UserId;
                entitySetupClass.UpdatedDate = DateTime.UtcNow;
                entitySetupClass.ClassName = model.ClassName;
                entitySetupClass.Status = model.Status;
                _ent.Entry(entitySetupClass).State = EntityState.Modified;
                _ent.Entry(entitySetupClass).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(entitySetupClass).Property(x => x.CreatedDate).IsModified = false;

            }
            else
            { 
            foreach (var item in model.SetupClassViewModelList)
            {
                if (model.ClassId == 0)
                {
                    var data = _ent.SetupClasses.Where(x => x.ClassName == item.ClassName.Trim() && x.DeletedDate == null).ToList();
                    if (data.Count > 0)
                    {
                        return 2;
                    }
                }

              
               
               
                    SetupClass entitySetupClass = new SetupClass();
                    entitySetupClass.ClassId =model.ClassId;
                    entitySetupClass.ClassName = item.ClassName;
                    entitySetupClass.Status = true;
                    entitySetupClass.CreatedBy = user.UserId;
                    entitySetupClass.CreatedDate = DateTime.UtcNow;
                    _ent.Entry(entitySetupClass).State = EntityState.Added;
                
                try
                {
                    _ent.SaveChanges();

                }
                catch (Exception ex)
                {

                    return 0;
                }

            }
            }
            _ent.SaveChanges();
            return 1;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMS.Models.SetupViewModel;
using SMS.SMS.Entities;
using System.Data.Entity;

namespace SMS.Service.Providers.SetupProviders
{
    public interface ISetupPaymentForClassProviders
    {
        SetupPaymentForClassViewModel GetList();
        SetupPaymentForClassViewModel Edit(int paymentForClassId);
        int Delete(int paymentForClassId);
        int Save(SetupPaymentForClassViewModel model);
    }

    public class SetupPaymentForClassProvider : ISetupPaymentForClassProviders
    {
        private readonly SMSEntities _ent;

        public SetupPaymentForClassProvider()
        {
            this._ent = new SMSEntities();
        }

        public SetupPaymentForClassViewModel GetList()
        {
            SetupPaymentForClassViewModel model = new SetupPaymentForClassViewModel();
            model.SetupPaymentForClassViewModelList = (from p in _ent.SetupPaymentForClasses
                                                       join c in _ent.SetupClasses on p.ClassId equals c.ClassId
                                                       where p.DeletedDate == null
                                                       select new SetupPaymentForClassViewModel
                                                       {
                                                           PaymentForClassId = p.PaymentForClassId,
                                                           ClassFee = p.ClassFee,
                                                           ClassName = c.ClassName,
                                                           Status = p.Status
                                                       }).ToList();
            return model;
        }

        public SetupPaymentForClassViewModel Edit(int paymentForClassId)
        {
            SetupPaymentForClassViewModel model = new SetupPaymentForClassViewModel();
            var data = _ent.SetupPaymentForClasses.Where(x => x.PaymentForClassId == paymentForClassId).FirstOrDefault();
            if (data != null)
            {
                model.PaymentForClassId = data.PaymentForClassId;
                model.ClassId = data.ClassId;
                model.ClassFee = data.ClassFee;
                model.Status = data.Status;
            }
            return model;
        }

        public int Save(SetupPaymentForClassViewModel model)
        {

            var setupPaymentForClassEntity = new SetupPaymentForClass();
            setupPaymentForClassEntity.PaymentForClassId = model.PaymentForClassId;
            setupPaymentForClassEntity.ClassId = model.ClassId;
            setupPaymentForClassEntity.ClassFee = model.ClassFee ?? 0;


            var data = _ent.SetupPaymentForClasses.Where(x => x.ClassFee == model.ClassFee && x.DeletedDate == null && x.ClassId == model.ClassId).ToList();
            if (data.Count > 0)
            {
                return 2;
            }

            var user = Userprovider.CustomeUserManager.GetUserDetails();
            if (model.PaymentForClassId > 0)
            {
                setupPaymentForClassEntity.UpdatedBy = user.UserId;
                setupPaymentForClassEntity.UpdatedDate = DateTime.UtcNow;
                setupPaymentForClassEntity.Status = model.Status;
                _ent.Entry(setupPaymentForClassEntity).State = EntityState.Modified;
                _ent.Entry(setupPaymentForClassEntity).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(setupPaymentForClassEntity).Property(x => x.CreatedDate).IsModified = false;
            }
            else
            {
                setupPaymentForClassEntity.CreatedDate = DateTime.UtcNow;
                setupPaymentForClassEntity.Status = true;
                setupPaymentForClassEntity.CreatedBy = user.UserId;
                _ent.Entry(setupPaymentForClassEntity).State = EntityState.Added;
            }
            try
            {
                _ent.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {

                return 0;
            }
        }

        public int Delete(int paymentForClassId)
        {
            var user = Userprovider.CustomeUserManager.GetUserDetails();
            var data = _ent.SetupPaymentForClasses.Where(x => x.PaymentForClassId == paymentForClassId).FirstOrDefault();
            if (data != null)
            {
                data.DeletedBy = user.UserId;
                data.DeletedDate = DateTime.UtcNow;
                _ent.Entry(data).State = EntityState.Modified;
                try
                {
                    _ent.SaveChanges();
                    return 1;
                }
                catch (Exception ex)
                {

                    return 0;
                }

            }
            return 0;
        }

    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMS.Models.SetupViewModel;
using SMS.SMS.Entities;
using System.Data.Entity;

namespace SMS.Service.Providers.SetupProviders
{
    public interface ISetupPostProvider
    {
        SetupPostViewModel GetPostList();
        int Save(SetupPostViewModel model);
        SetupPostViewModel Edit(int postId);
        int Delete(int postId);

    }
    public class SetupPostProvider: ISetupPostProvider
    {
        private readonly SMSEntities _ent;
        public SetupPostProvider()
        {
            this._ent = new SMSEntities();
        }

        public SetupPostViewModel GetPostList()
        {
            SetupPostViewModel model = new SetupPostViewModel();
            model.SetupPostViewModelList = (from p in _ent.SetupPosts
                                            join s in _ent.SetupServices on p.ServiceId equals s.ServiceId
                                            where p.DeletedDate == null
                                            select new SetupPostViewModel
                                            {
                                                PostId=p.PostId,
                                                ServiceName=s.ServiceName,
                                                PostName=p.PostName,
                                                PostOrder=p.PostOrder,
                                                Status=p.Status??false
                                            }
                                            ).ToList();
            return model;
        }

        public int Save(SetupPostViewModel model)
        {
            //return 0:save failed
            //retur 1:save success
            //return 2 already exit
            SetupPost setupPostEntity = new SetupPost();
            setupPostEntity.PostId = model.PostId;
            setupPostEntity.ServiceId = model.ServiceId;
            setupPostEntity.PostName = model.PostName;
            setupPostEntity.PostOrder = model.PostOrder;

            var data = _ent.SetupPosts.Where(x => x.PostName == model.PostName.Trim() && x.DeletedDate==null && x.ServiceId==model.ServiceId).ToList();
            if (data.Count > 0)
            {
                return 2;
            }
            var user = Userprovider.CustomeUserManager.GetUserDetails();
            if (model.PostId > 0)
            {
                setupPostEntity.Status = model.Status;
                setupPostEntity.UpdatedBy = user.UserId;
                setupPostEntity.UpdatedDate = DateTime.UtcNow;
                _ent.Entry(setupPostEntity).State = EntityState.Modified;
                _ent.Entry(setupPostEntity).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(setupPostEntity).Property(x => x.CreatedDate).IsModified = false;
            }
            else
            {
                setupPostEntity.Status = true;
                setupPostEntity.CreatedBy = user.UserId;
                setupPostEntity.CreatedDate = DateTime.UtcNow;
                _ent.Entry(setupPostEntity).State = EntityState.Added;
            }
            try
            {
                _ent.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {

                return 0;
            }
        }

        public SetupPostViewModel Edit(int postId)
        {
            SetupPostViewModel model = new SetupPostViewModel();
            var data = _ent.SetupPosts.Where(x => x.PostId == postId).FirstOrDefault();
            if (data != null)
            {

                model.PostId = data.PostId;
                model.ServiceId = data.ServiceId;
                model.PostName = data.PostName;
                model.PostOrder = data.PostOrder;
                model.Status = data.Status ?? false;
            }
            return model;
        }

        public int Delete(int postId)
        {
            var data = _ent.SetupPosts.Where(x => x.PostId == postId).FirstOrDefault();
            if (data != null)
            {
                var user = Userprovider.CustomeUserManager.GetUserDetails();
                data.DeletedBy = user.UserId;
                data.DeletedDate = DateTime.UtcNow;
                _ent.Entry(data).State = EntityState.Modified;
                try
                {
                    _ent.SaveChanges();
                    return 1;
                }
                catch (Exception ex)
                {

                    return 0;
                }

            }
            return 0;
        }
    }
}

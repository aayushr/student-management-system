﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMS.Models.SetupViewModel;
using System.Data.Entity;
using SMS.SMS.Entities;

namespace SMS.Service.Providers.SetupProviders
{
        public interface ISetupServiceProvider
        {
            int Save(SetupServiceViewModel model);
            SetupServiceViewModel Edit(int id);
            int Delete(int id);
            SetupServiceViewModel GetServiceList();
        }
        public class SetupServiceProvider : ISetupServiceProvider
        {
            private readonly SMSEntities _ent;
            public SetupServiceProvider()
            {
                this._ent = new SMSEntities();
            }
            public int Delete(int id)
            {
                var data = _ent.SetupServices.Where(x => x.ServiceId == id).FirstOrDefault();
                if (data != null)
                {
                var user = Userprovider.CustomeUserManager.GetUserDetails();
                data.DeletedBy = user.UserId;
                    data.DeletedDate = DateTime.UtcNow;
                    _ent.Entry(data).State = EntityState.Modified;
                    _ent.SaveChanges();
                }
            try
            {
                return 1;
            }
            catch (Exception ee)
            {

                return 0;
            }

            }

            public SetupServiceViewModel Edit(int id)
            {
                SetupServiceViewModel model = new SetupServiceViewModel();
                var data = _ent.SetupServices.Where(x => x.ServiceId == id).FirstOrDefault();
                if (data != null)
                {
                    model.ServiceId = data.ServiceId;
                    model.ServiceName = data.ServiceName;
                    model.ServiceOrder = data.ServiceOrder;
                    model.Status = data.Status??false;
                
                }
                return model;
            }

            public SetupServiceViewModel GetServiceList()
            {
                SetupServiceViewModel model = new SetupServiceViewModel();
                model.SetupServiceViewModelList = (from s in _ent.SetupServices
                                                   where s.DeletedDate == null
                                                   select new SetupServiceViewModel
                                                   {
                                                       ServiceId=s.ServiceId,
                                                       ServiceName = s.ServiceName,
                                                       ServiceOrder = s.ServiceOrder,
                                                       Status=s.Status??false
                                                   }).ToList();
                return model;
            }

            public int Save(SetupServiceViewModel model)
            {
            var setupserviceEntity = new SetupService();
            if (model.ServiceId==0)
            {
                var data = _ent.SetupServices.Where(x => x.ServiceName == model.ServiceName.Trim() && x.DeletedDate == null).ToList();
                if(data.Count>0)
                {
                    return 2;
                }
            }
            var user = Userprovider.CustomeUserManager.GetUserDetails();
            setupserviceEntity.ServiceId = model.ServiceId;
            setupserviceEntity.ServiceName = model.ServiceName;
            setupserviceEntity.ServiceOrder = model.ServiceOrder;
            if (model.ServiceId > 0)
            {
                setupserviceEntity.UpdatedBy = user.UserId;
                setupserviceEntity.UpdatedDate = DateTime.UtcNow;
                _ent.Entry(setupserviceEntity).State = EntityState.Modified;
                _ent.Entry(setupserviceEntity).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(setupserviceEntity).Property(x => x.CreatedDate).IsModified = false;
            }
            else
            {
                setupserviceEntity.Status = true;
                setupserviceEntity.CreatedBy = user.UserId;
                setupserviceEntity.CreatedDate = DateTime.UtcNow;
                _ent.Entry(setupserviceEntity).State = EntityState.Added;
               
            }
            try
            {
                _ent.SaveChanges();
                return 1;
            }
            catch (Exception ee)
            {

                return 0;
            }
            
         
            }

        }



    
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMS.SMS.Entities;
using SMS.Models.SetupViewModel;
using System.Data.Entity;

namespace SMS.Service.Providers.SetupProviders
{
    
    public interface  ISetupPaymentForHostelProvider
    {
        int Save(SetupPaymentForHostelViewModel model);
        SetupPaymentForHostelViewModel Edit(int paymentForHostelId);
        int Delete(int paymentForHostelId);
        SetupPaymentForHostelViewModel GetPaymentForHostelList();


    }
    public class SetupPaymentForHostelProvider: ISetupPaymentForHostelProvider
    {
        SMSEntities _ent = new SMSEntities();
        public SetupPaymentForHostelProvider()
        {
            _ent = new SMSEntities();
        }

        public int Delete(int paymentForHostelId)
        {
            var user = Userprovider.CustomeUserManager.GetUserDetails();
            var data = _ent.SetupPaymentForHostels.Where(x => x.PaymentForHostelId == paymentForHostelId && x.DeletedDate==null).FirstOrDefault();
            if(data!=null)
            {
                data.DeletedBy = user.UserId;
                data.DeletedDate = DateTime.UtcNow;
                _ent.Entry(data).State = EntityState.Modified;
                _ent.SaveChanges();
                try
                {
                    return 1;
                }
                catch (Exception ee)
                {
                   
                    return 0;
                }
            }
            return 0;
        }

        public SetupPaymentForHostelViewModel Edit(int paymentForHostelId)
        {
            SetupPaymentForHostelViewModel model = new SetupPaymentForHostelViewModel();
            var data = _ent.SetupPaymentForHostels.Where(x => x.PaymentForHostelId == paymentForHostelId).FirstOrDefault();
            if(data!=null)
            {
                model.PaymentForHostelId = data.PaymentForHostelId;
                model.HostelName = data.HostelName;
                model.ClassId = data.ClassId??0;
                model.FacultyId = data.FacultyId??0;
                model.HostelFee = data.HostelFee;
                model.Status = data.Status??false;

            }
            return model;
        }

        public SetupPaymentForHostelViewModel GetPaymentForHostelList()
        {
            SetupPaymentForHostelViewModel model = new SetupPaymentForHostelViewModel();
            model.SetupPaymentForHostelViewModelList = (from s in _ent.SetupPaymentForHostels
                                                        join c in _ent.SetupClasses on s.ClassId equals c.ClassId
                                                        join f in _ent.SetupFaculties on s.FacultyId equals f.FacultyId
                                                        where s.DeletedDate == null
                                                        select new SetupPaymentForHostelViewModel
                                                        {
                                                            PaymentForHostelId = s.PaymentForHostelId,
                                                            FacultyName=f.FacultyName,
                                                            ClassName=c.ClassName,
                                                            HostelName=s.HostelName,
                                                            ClassId=s.ClassId??0,
                                                            FacultyId=s.FacultyId??0,
                                                            HostelFee=s.HostelFee,
                                                            Status=s.Status??false

                                                        }).ToList();
            return model;
        }

        public int Save(SetupPaymentForHostelViewModel model)
        {
            SetupPaymentForHostel setupPaymentForHostelEntity = new SetupPaymentForHostel();
            var data = _ent.SetupPaymentForHostels.Where(x => x.HostelName == model.HostelName.Trim() && x.DeletedDate == null && x.ClassId==model.ClassId&&x.FacultyId==model.FacultyId).ToList();
            if(data.Count > 0)
            {
                return 2;
            }
            var user = Userprovider.CustomeUserManager.GetUserDetails();
            setupPaymentForHostelEntity.HostelName = model.HostelName;
            setupPaymentForHostelEntity.ClassId = model.ClassId;
            setupPaymentForHostelEntity.FacultyId = model.FacultyId;
            setupPaymentForHostelEntity.HostelFee = model.HostelFee;
            if(model.PaymentForHostelId > 0)
            {
                setupPaymentForHostelEntity.PaymentForHostelId = model.PaymentForHostelId;
                setupPaymentForHostelEntity.Status = model.Status;
                setupPaymentForHostelEntity.UpdatedBy = user.UserId;
                setupPaymentForHostelEntity.UpdatedDate = DateTime.UtcNow;
                _ent.Entry(setupPaymentForHostelEntity).State = EntityState.Modified;
                _ent.Entry(setupPaymentForHostelEntity).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(setupPaymentForHostelEntity).Property(x => x.CreatedDate).IsModified = false;
            }
            else
            {
                setupPaymentForHostelEntity.Status = true;
                setupPaymentForHostelEntity.CreatedBy = user.UserId;
                setupPaymentForHostelEntity.CreatedDate = DateTime.UtcNow;
                _ent.Entry(setupPaymentForHostelEntity).State = EntityState.Added;
            }
            try
            {
                _ent.SaveChanges();
                return 1;
            }
            catch (Exception ee)
            {

                return 0;
            }
            
          
        }
    }
}

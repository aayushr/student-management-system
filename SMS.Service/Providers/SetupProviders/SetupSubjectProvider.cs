﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMS.Models.SetupViewModel;
using SMS.SMS.Entities;
using System.Data.Entity;

namespace SMS.Service.Providers.SetupProviders
{
    public interface ISetupSubjectProvider
    {
        int Save(SetupSubjectViewModel model);
        SetupSubjectViewModel Edit(int subjectId);
        int Delete(int subjectId);
        List<SetupSubjectViewModel> GetSubjectList();


    }
    public class SetupSubjectProvider : ISetupSubjectProvider
    {
        private readonly SMSEntities _ent;
        public SetupSubjectProvider()
        {
            this._ent = new SMSEntities();
        }

        public int Delete(int subjectId)
        {
            SetupSubjectViewModel model = new SetupSubjectViewModel();
            var data = _ent.SetupSubjects.Where(x => x.SubjectId == subjectId).SingleOrDefault();
            if (data != null)
            {
                var user = Userprovider.CustomeUserManager.GetUserDetails();
                data.DeletedBy = user.UserId;
                data.DeletedDate = DateTime.UtcNow;
                _ent.Entry(data).State = EntityState.Modified;
                _ent.SaveChanges();
            }
            try
            {
                return 1;
            }
            catch (Exception ee)
            {

                return 0;
            }
            
        }

        public SetupSubjectViewModel Edit(int id)
        {
            SetupSubjectViewModel model = new SetupSubjectViewModel();
            var data = _ent.SetupSubjects.Where(x => x.SubjectId == id).FirstOrDefault();
            if (data != null)
            {
                model.SubjectId = data.SubjectId;
                model.SubjectName = data.SubjectName;
                model.IsOptional = data.IsOptional ?? false;
                model.Status = data.Status;
            }
            return model;
        }

        public int Save(SetupSubjectViewModel model)
        {
            var setupSubjectEntity = new SetupSubject();
            if (model.SubjectId==0)
            {
                var data = _ent.SetupSubjects.Where(x => x.SubjectName == model.SubjectName.Trim() && x.DeletedDate == null).ToList();
                if(data.Count>0)
                {
                    return 2;
                }
            }
            var user = Userprovider.CustomeUserManager.GetUserDetails();
            setupSubjectEntity.SubjectId = model.SubjectId;
            setupSubjectEntity.SubjectName = model.SubjectName;
            setupSubjectEntity.IsOptional = model.IsOptional;
            if (model.SubjectId > 0)
            {
                setupSubjectEntity.Status = model.Status;
                setupSubjectEntity.UpdatedDate = DateTime.UtcNow;
                setupSubjectEntity.UpdatedBy = user.UserId;
                _ent.Entry(setupSubjectEntity).State = EntityState.Modified;
                _ent.Entry(setupSubjectEntity).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(setupSubjectEntity).Property(x => x.CreatedDate).IsModified = false;
            }
            else
            {
                setupSubjectEntity.Status = true;
                setupSubjectEntity.CreatedDate = DateTime.UtcNow;
                setupSubjectEntity.CreatedBy = user.UserId;
                _ent.Entry(setupSubjectEntity).State = System.Data.Entity.EntityState.Added;
            }
            try
            {
                _ent.SaveChanges();
                return 1;
            }
            catch (Exception ee)
            {

                return 0;
            }
        }

        public List<SetupSubjectViewModel> GetSubjectList()
        {
            List<SetupSubjectViewModel> list = new List<Models.SetupViewModel.SetupSubjectViewModel>();

            list = (from s in _ent.SetupSubjects
                    where s.DeletedDate == null
                    select new SetupSubjectViewModel
                    {
                        SubjectId = s.SubjectId,
                        SubjectName = s.SubjectName,
                        IsOptional = s.IsOptional ?? false,
                        Status=s.Status
                        

                    }).ToList();
            return list;

        }
    }
}

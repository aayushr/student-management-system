﻿using SMS.Models.SetupViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMS.SMS.Entities;
using System.Data.Entity;

namespace SMS.Service.Providers.SetupProviders
{
    public interface ISetupSectionProvider
    {
        int Save(SetupSectionViewModel model);
        SetupSectionViewModel Edit(int sectionId);
        int Delete(int sectionId);
        SetupSectionViewModel GetList();

    }
    public class SetupSectionProvider : ISetupSectionProvider
    {
        private readonly SMSEntities _ent;
        public SetupSectionProvider()
        {
            this._ent = new SMSEntities();
        }

        public SetupSectionViewModel GetList()
        {
            SetupSectionViewModel model = new SetupSectionViewModel();
            model.SetupSectionViewModelList = (from ss in _ent.SetupSections
                                               join c in _ent.SetupClasses on ss.ClassId equals c.ClassId
                                               where ss.DeletedDate == null
                                               select new SetupSectionViewModel
                                               {
                                                   SectionId = ss.SectionId,
                                                   ClassName=c.ClassName,
                                                   SectionName = ss.SectionName,
                                                   Status = ss.Status??false
                                               }).ToList();
            return model;
        }
        public int Save(SetupSectionViewModel model)
        {
            var setupSectionEntity = new SetupSection();
            setupSectionEntity.SectionName = model.SectionName;
            setupSectionEntity.SectionId = model.SectionId;
            setupSectionEntity.ClassId = model.ClassId;

            var data = _ent.SetupSections.Where(x => x.SectionName == model.SectionName.Trim() && x.DeletedDate == null && x.ClassId==model.ClassId).ToList();

            if (data.Count > 0)
            {
                return 2;
            }
            var user = Userprovider.CustomeUserManager.GetUserDetails();
            if (model.SectionId > 0)
            {
                setupSectionEntity.Status = model.Status;
                setupSectionEntity.UpdatedBy = user.UserId;
                setupSectionEntity.UpdatedDate = DateTime.UtcNow;
                _ent.Entry(setupSectionEntity).State = EntityState.Modified;
                _ent.Entry(setupSectionEntity).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(setupSectionEntity).Property(x => x.CreatedDate).IsModified = false;
            }
            else
            {
                setupSectionEntity.Status = true;
                setupSectionEntity.CreatedBy = user.UserId;
                setupSectionEntity.CreatedDate = DateTime.UtcNow;
                _ent.Entry(setupSectionEntity).State = EntityState.Added;
            }
            try
            {
                _ent.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {

                return 0;
            }
        }

        public SetupSectionViewModel Edit(int sectionId)
        {
            SetupSectionViewModel model = new SetupSectionViewModel();
            var data = _ent.SetupSections.Where(x => x.SectionId == sectionId).FirstOrDefault();
            if (data != null)
            {
                model.SectionId = data.SectionId;
                model.ClassId = data.ClassId;
                model.SectionName = data.SectionName;
                model.Status = data.Status??false;
            }
            return model;
        }

        public int Delete(int sectionId)
        {
            var data = _ent.SetupSections.Where(x => x.SectionId == sectionId).FirstOrDefault();
            if (data != null)
            {
                var user = Userprovider.CustomeUserManager.GetUserDetails();
                data.DeletedBy = user.UserId;
                data.DeletedDate = DateTime.UtcNow;
                _ent.Entry(data).State = EntityState.Modified;
                try
                {
                    _ent.SaveChanges();
                    return 1;
                }
                catch (Exception ex)
                {

                    return 0;
                }
            }
            return 0;
        }

    }
}

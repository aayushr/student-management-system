﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMS.SMS.Entities;
using SMS.Models.SetupViewModel;
using System.Data.Entity;


namespace SMS.Service.Providers.SetupProviders
{
   
   public interface ISetupPaymentForLibraryProvider
    {
        int Save(SetupPaymentForLibraryViewModel model);
        SetupPaymentForLibraryViewModel Edit(int paymentForLibraryId);
        int Delete(int paymentForLibraryId);
        SetupPaymentForLibraryViewModel GetPaymentForLibraryList();


    }
    public class SetupPaymentForLibraryProvider: ISetupPaymentForLibraryProvider
    {
        private readonly SMSEntities _ent;
        public SetupPaymentForLibraryProvider()
        {
            this._ent = new SMSEntities();

        }

        public int Delete(int paymentForLibraryId)
        {
            var user = Userprovider.CustomeUserManager.GetUserDetails();
            var data = _ent.SetupPaymentForLibraries.Where(x => x.PaymentForLibraryId == paymentForLibraryId).FirstOrDefault();
            if (data != null)
            {
                data.DeletedBy = user.UserId;
                data.DeletedDate = DateTime.UtcNow;
                _ent.Entry(data).State = EntityState.Modified;
                _ent.SaveChanges();

                try
                {
                    _ent.SaveChanges();
                    return 1;
                }
                catch (Exception ex)
                {

                    return 0;
                }

            }
            return 0;
       
        }

    public SetupPaymentForLibraryViewModel Edit(int paymentForLibraryId)
        {
            SetupPaymentForLibraryViewModel model = new SetupPaymentForLibraryViewModel();
            var data = _ent.SetupPaymentForLibraries.Where(x => x.PaymentForLibraryId == paymentForLibraryId).FirstOrDefault();
            if(data!=null)
            {
                model.PaymentForLibraryId = data.PaymentForLibraryId;
                model.LibraryName = data.LibraryName;
                model.ClassId = data.ClassId;
                model.FacultyId = data.FacultyId;
                model.LibraryFee = data.LibraryFee;
                model.Status = data.Status??false;

            }
            return model;
            
        }

        public SetupPaymentForLibraryViewModel GetPaymentForLibraryList()
        {
            SetupPaymentForLibraryViewModel model = new SetupPaymentForLibraryViewModel();
            model.SetupPaymentForLibraryViewModelList = (from p in _ent.SetupPaymentForLibraries 
                                                         join  c in _ent.SetupClasses on p.ClassId equals c.ClassId
                                                         join f in _ent.SetupFaculties on p.FacultyId equals f.FacultyId
                                                         where p.DeletedDate == null
                                                         select new SetupPaymentForLibraryViewModel
                                                         {
                                                             PaymentForLibraryId = p.PaymentForLibraryId,
                                                             LibraryName = p.LibraryName,
                                                             ClassName=c.ClassName,
                                                             FacultyName=f.FacultyName,
                                                             ClassId = p.ClassId,
                                                             FacultyId = p.FacultyId,
                                                             LibraryFee=p.LibraryFee,
                                                             Status=p.Status??false
                                                         }).ToList();
            return model;
        }

        public int Save(SetupPaymentForLibraryViewModel model)
        {

            SetupPaymentForLibrary setupPaymentForLibraryEntity = new SetupPaymentForLibrary();
            var data = _ent.SetupPaymentForLibraries.Where(x => x.LibraryName == model.LibraryName.Trim() && x.ClassId==model.ClassId && x.FacultyId==model.FacultyId).ToList();
            if (data.Count > 0)
            {
                return 2;
            }
            var user = Userprovider.CustomeUserManager.GetUserDetails();
            setupPaymentForLibraryEntity.LibraryName = model.LibraryName;
            setupPaymentForLibraryEntity.ClassId = model.ClassId;
            setupPaymentForLibraryEntity.FacultyId = model.FacultyId;
            setupPaymentForLibraryEntity.LibraryFee = model.LibraryFee;
            if(model.PaymentForLibraryId > 0)
            {
                setupPaymentForLibraryEntity.Status = model.Status;
                setupPaymentForLibraryEntity.PaymentForLibraryId = model.PaymentForLibraryId;
                setupPaymentForLibraryEntity.UpdatedBy = user.UserId;
                setupPaymentForLibraryEntity.UpdatedDate = DateTime.UtcNow;
                _ent.Entry(setupPaymentForLibraryEntity).State = EntityState.Modified;
                _ent.Entry(setupPaymentForLibraryEntity).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(setupPaymentForLibraryEntity).Property(x => x.CreatedDate).IsModified = false;

            }
            else
            {
                setupPaymentForLibraryEntity.Status = true;
                setupPaymentForLibraryEntity.CreatedBy = user.UserId;
                setupPaymentForLibraryEntity.CreatedDate = DateTime.UtcNow;
                _ent.Entry(setupPaymentForLibraryEntity).State = EntityState.Added;
            }
            try
            {
                _ent.SaveChanges();
                return 1;
            }
            catch (Exception ee)
            {

                return 0;
            }
        }
    }
}

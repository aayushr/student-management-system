﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMS.SMS.Entities;
using SMS.Models.SetupViewModel;
using System.Data.Entity;

namespace SMS.Service.Providers.SetupProviders
{

    public interface ISetupDepartmentProvider
    {
        int Save(SetupDepartmentViewModel model);
        SetupDepartmentViewModel Edit(int departmentId);
        SetupDepartmentViewModel GetDepartmentList();
        int Delete(int departmentId);

    }
    public class SetupDepartmentProvider : ISetupDepartmentProvider
    {

        public readonly SMSEntities _ent;
        public SetupDepartmentProvider()
        {
            this._ent = new SMSEntities();
        }

        public int Delete(int departmentId)
        {
            var user = Userprovider.CustomeUserManager.GetUserDetails();
            var data = _ent.SetupDepartments.Where(x => x.DepartmentId == departmentId).FirstOrDefault();
            if (data != null)
            {
                data.DeletedBy = user.UserId;
                data.DeletedDate = DateTime.UtcNow;
                _ent.Entry(data).State = EntityState.Modified;
                try
                {
                    _ent.SaveChanges();
                    return 1;
                }
                catch (Exception ex)
                {

                    return 0;
                }

            }
            return 0;

        }

        public SetupDepartmentViewModel Edit(int departmentId)
        {

            SetupDepartmentViewModel model = new SetupDepartmentViewModel();
            var data = _ent.SetupDepartments.Where(x => x.DepartmentId == departmentId).FirstOrDefault();
            if (data != null)
            {
                model.DepartmentId = data.DepartmentId;
                model.DepartmentName = data.DepartmentName;
                model.Status = data.Status ?? false;

            }
            return model;
        }

        public SetupDepartmentViewModel GetDepartmentList()
        {
            SetupDepartmentViewModel model = new SetupDepartmentViewModel();
            model.SetupDepartmentViewModelList = (from d in _ent.SetupDepartments
                                                  where d.DeletedDate == null
                                                  select new SetupDepartmentViewModel
                                                  {
                                                      DepartmentId = d.DepartmentId,
                                                      DepartmentName = d.DepartmentName,
                                                      Status = d.Status ?? false
                                                  }).ToList();
            return model;
        }

        public int Save(SetupDepartmentViewModel model)
        {
            var user = Userprovider.CustomeUserManager.GetUserDetails();
            SetupDepartment setupDepartmentEntity = new SetupDepartment();
            setupDepartmentEntity.DepartmentName = model.DepartmentName;

            var data = _ent.SetupDepartments.Where(x => x.DepartmentName == model.DepartmentName.Trim() && x.DeletedDate==null).ToList();
            if (data.Count > 0)
            {
                return 2;
            }
            if (model.DepartmentId > 0)
            {
                setupDepartmentEntity.Status = model.Status;
                setupDepartmentEntity.UpdatedBy = user.UserId;
                setupDepartmentEntity.DepartmentId = model.DepartmentId;
                setupDepartmentEntity.UpdatedDate = DateTime.UtcNow;
                _ent.Entry(setupDepartmentEntity).State = EntityState.Modified;
                _ent.Entry(setupDepartmentEntity).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(setupDepartmentEntity).Property(x => x.CreatedDate).IsModified = false;

            }
            else
            {
                setupDepartmentEntity.Status = true;
                setupDepartmentEntity.CreatedBy = user.UserId;
                setupDepartmentEntity.CreatedDate = DateTime.UtcNow;
                _ent.Entry(setupDepartmentEntity).State = EntityState.Added;
            }
            try
            {
                _ent.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {

                return 0;
            }


        }
    }
}

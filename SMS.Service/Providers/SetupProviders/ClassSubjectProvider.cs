﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMS.Models.SetupViewModel;
using SMS.SMS.Entities;
using System.Data.Entity;

namespace SMS.Service.Providers.SetupProviders
{
    public interface IClassSubjectProvider
    {
        int  Save(ClassSubjectViewModel model);
        ClassSubjectViewModel Edit(int classSubjectId);
        int Delete(int classSubjectId);
        ClassSubjectViewModel GetClassSubjectList();
    }

    public class ClassSubjectProvider : IClassSubjectProvider
    {
        private readonly SMSEntities _ent;
        public ClassSubjectProvider()
        {
            this._ent = new SMSEntities();
        }
        public int Delete(int classSubjectId)
        {
            var user = Userprovider.CustomeUserManager.GetUserDetails();
            var data = _ent.ClassSubjects.Where(x => x.ClassSubjectId == classSubjectId).FirstOrDefault();
            if (data != null)
            {
                data.DeletedBy = user.UserId;
                data.DeletedDate = DateTime.UtcNow;
                _ent.Entry(data).State = EntityState.Modified;
                _ent.SaveChanges();
                try
                {
                    return 1;
                }
                catch (Exception ee)
                {

                    return 0;
                }
            }
            return 0;
        }

        public ClassSubjectViewModel Edit(int classSubjectId)
        {
            ClassSubjectViewModel model = new ClassSubjectViewModel();
            var data = _ent.ClassSubjects.Where(x => x.ClassSubjectId == classSubjectId).FirstOrDefault();
            if(data!=null)
            {
                model.ClassSubjectId = data.ClassSubjectId;
                model.ClassId = data.ClassId;
                model.SubjectId = data.SubjectId;
                model.Status = data.Status??false;
            }
            return model;
        }

        public ClassSubjectViewModel GetClassSubjectList()
        {
            ClassSubjectViewModel model = new ClassSubjectViewModel();
            model.ClassSubjectViewModelList = (from cs in _ent.ClassSubjects
                                               join c in _ent.SetupClasses on cs.ClassId equals c.ClassId
                                               join s in _ent.SetupSubjects on cs.SubjectId equals s.SubjectId
                                               where cs.DeletedDate == null
                                               select new ClassSubjectViewModel
                                               {
                                                   ClassSubjectId = cs.ClassSubjectId,
                                                   ClassName=c.ClassName,
                                                   SubjectName=s.SubjectName,
                                                   ClassId=cs.ClassId,
                                                   SubjectId=cs.SubjectId,
                                                   Status=cs.Status??false
                                                   

                                               }).ToList();
            return model;
        }

        public int Save(ClassSubjectViewModel model)
        {
            ClassSubject classSubjectEntity = new ClassSubject();
            var user = Userprovider.CustomeUserManager.GetUserDetails();
            foreach (var item in model.SetupSubjectViewModelList)
            {
                classSubjectEntity.ClassId = model.ClassId;
                classSubjectEntity.SubjectId = item.SubjectId;
                if (model.ClassSubjectId > 0)
                {
                    classSubjectEntity.Status = model.Status;
                    classSubjectEntity.ClassSubjectId = model.ClassSubjectId;
                    classSubjectEntity.UpdatedBy = user.UserId;
                    classSubjectEntity.UpdatedDate = DateTime.UtcNow;
                    _ent.Entry(classSubjectEntity).State = EntityState.Modified;
                    _ent.Entry(classSubjectEntity).Property(x => x.CreatedBy).IsModified = false;
                    _ent.Entry(classSubjectEntity).Property(x => x.CreatedDate).IsModified = false;
                }
                else
                {
                    classSubjectEntity.Status = true;
                    classSubjectEntity.CreatedBy = user.UserId;
                    classSubjectEntity.CreatedDate = DateTime.UtcNow;
                    _ent.Entry(classSubjectEntity).State = EntityState.Added;
                }
                try
                {
                    _ent.SaveChanges();
                    
                }
                catch (Exception ee)
                {

                    return 0;
                }
            }
            return 0;
        }
    }
}

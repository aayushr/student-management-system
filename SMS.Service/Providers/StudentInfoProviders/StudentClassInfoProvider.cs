﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMS.Models.StudentInfoViewModel;
using SMS;
using SMS.SMS.Entities;
using System.Data.Entity;

namespace SMS.Service.Providers.StudentInfoProviders
{
    public interface IStudentClassInfoProvider
    {
        int Save(StudentClassInfoViewModel model);
        StudentClassInfoViewModel Edit(int studentClasssId);
        int Delete(int studentClasssId);
        StudentClassInfoViewModel GetList();
    }

    public class StudentClassInfoProvider : IStudentClassInfoProvider
    {
        private readonly SMSEntities _ent;
        public StudentClassInfoProvider()
        {
            this._ent = new SMSEntities();
        }

        public StudentClassInfoViewModel GetList()
        {
            StudentClassInfoViewModel model = new StudentClassInfoViewModel();
            model.StudentClassInfoViewModelList = (from sc in _ent.StudentClassInfoes
                                                   join s in _ent.StudentInfoes on sc.StudentId equals s.StudentId
                                                   join c in _ent.SetupClasses on sc.ClassId equals c.ClassId
                                                   join se in _ent.SetupSections on sc.SectionId equals se.SectionId
                                                   join b in _ent.SetupBlocks on sc.BlockId equals b.BlockId
                                                   join f in _ent.SetupFaculties on sc.FacultyId equals f.FacultyId
                                                   where sc.DeletedDate == null
                                                   select new StudentClassInfoViewModel
                                                   {
                                                       StudentClassId = sc.StudentClassId,
                                                       StudentName = s.FirstName,
                                                       ClassName = c.ClassName,
                                                       SectionName = se.SectionName,
                                                       BlockName = b.BlockName,
                                                       FacultyName=f.FacultyName,
                                                       RollNo = sc.RollNo,
                                                       Status = sc.Status??false
                                                   }
                                                 ).ToList();
            return model;
        }

        public int Save(StudentClassInfoViewModel model)
        {
            var studentClassInfoEntity = new StudentClassInfo();
            studentClassInfoEntity.StudentClassId = model.StudentClassId;
            studentClassInfoEntity.StudentId = model.StudentId;
            studentClassInfoEntity.ClassId = model.ClassId;
            studentClassInfoEntity.SectionId = model.SectionId;
            studentClassInfoEntity.BlockId = model.BlockId;
            studentClassInfoEntity.FacultyId = model.FacultyId??0;
            studentClassInfoEntity.RollNo = model.RollNo;
            studentClassInfoEntity.Status = model.Status;

            var data = _ent.StudentClassInfoes.Where(x => x.RollNo == model.RollNo.Trim() && x.DeletedDate==null && x.StudentId==model.StudentId && x.ClassId==model.ClassId && x.SectionId==model.SectionId && x.BlockId==model.BlockId && x.FacultyId==model.FacultyId).ToList();
            if(data.Count >0)
            {
                return 2;
            }
            var user = Userprovider.CustomeUserManager.GetUserDetails();
            if (model.StudentClassId > 0)
            {
                studentClassInfoEntity.UpdatedDate = DateTime.UtcNow;
                studentClassInfoEntity.UpdatedBy = user.UserId;
                _ent.Entry(studentClassInfoEntity).State = EntityState.Modified;
                _ent.Entry(studentClassInfoEntity).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(studentClassInfoEntity).Property(x => x.CreatedDate).IsModified = false;
            }
            else
            {
                studentClassInfoEntity.Status = true;
                studentClassInfoEntity.CreatedDate = DateTime.UtcNow;
                studentClassInfoEntity.CreatedBy = user.UserId;
                _ent.Entry(studentClassInfoEntity).State = EntityState.Added;
            }
            try
            {
                _ent.SaveChanges();
                return 1;
            }
            catch (Exception ee)
            {

                return 0;
            }
        
        }

        public StudentClassInfoViewModel Edit(int studentClassId)
        {
            StudentClassInfoViewModel model = new StudentClassInfoViewModel();
            var data = _ent.StudentClassInfoes.Where(x => x.StudentClassId == studentClassId).FirstOrDefault();
            if (data != null)
            {
                model.StudentClassId = data.StudentClassId;
                model.StudentId = data.StudentId;
                model.ClassId = data.ClassId;
                model.SectionId = data.SectionId;
                model.BlockId = data.BlockId;
                model.FacultyId = data.FacultyId;
                model.RollNo = data.RollNo;
                model.Status = data.Status??false;
            }
            return model;
        }

        public int Delete(int studentClassId)
        {
            var data = _ent.StudentClassInfoes.Where(x => x.StudentClassId == studentClassId).FirstOrDefault();
            if (data != null)
            {
                var user = Userprovider.CustomeUserManager.GetUserDetails();
                data.DeletedBy = user.UserId;
                data.DeletedDate = DateTime.Now;
                _ent.Entry(data).State = EntityState.Modified;
                try
                {
                    _ent.SaveChanges();
                    return 1;
                }
                catch (Exception ex)
                {

                    return 0;
                }
            }
            return 0;
        }

    }
}

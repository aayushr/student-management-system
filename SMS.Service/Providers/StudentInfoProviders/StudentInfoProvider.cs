﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMS.SMS.Entities;
using SMS;
using SMS.Models.StudentInfoViewModel;
using System.Data.Entity;
//coment
namespace SMS.Service.Providers.StudentInfoProvider
{
    public interface IStudentInfoProvider
    {
        int Save(StudentInfoViewModel model);
        StudentInfoViewModel Edit(int studentId);
        int Delete(int studentId);
        StudentInfoViewModel GetList();
        bool InitRegistration(StudentInfoViewModel model);
    }
    public class StudentInfoProvider : IStudentInfoProvider
    {
        private readonly SMSEntities _ent;
        public StudentInfoProvider()
        {
            this._ent = new SMSEntities();
        }


        public StudentInfoViewModel GetList()
        {
            StudentInfoViewModel model = new StudentInfoViewModel();
            model.StudentInfoViewModelList = (from s in _ent.StudentInfoes
                                              where s.DeletedDate == null
                                              select new StudentInfoViewModel
                                              {
                                                  StudentId = s.StudentId,
                                                  ClassId = s.ClassId ?? 0,
                                                  FacultyId = s.FacultyId ?? 0,
                                                  IsNew = s.IsNew ?? false,
                                                  Reference = s.Reference,
                                                  StudentCode = s.StudentCode,
                                                  FirstName = s.FirstName,
                                                  MidName = s.MidName,
                                                  LastName = s.LastName,
                                                  StudentType = s.StudentType,
                                                  IsTransportation = s.IsTransportation ?? false,
                                                  Religion = s.Religion,
                                                  PState = s.PState,
                                                  PDistrict = s.PDistrict,
                                                  PMetropolitan = s.PMetropolitan,
                                                  PSubMetropolitan = s.PSubMetropolitan,
                                                  PMunicipality = s.PMunicipality,
                                                  PGauPalika = s.PGauPalika,
                                                  PWardNo = s.PWardNo,
                                                  TState = s.TState,
                                                  TDistrict = s.TDistrict,
                                                  TMetropolitan = s.TMetropolitan,
                                                  TSubMetropolitan = s.TSubMetropolitan,
                                                  TMunicipality = s.TMunicipality,
                                                  TGauPalika = s.TGauPalika,
                                                  TWardNo = s.TWardNo,
                                                  BloodGroup = s.BloodGroup,
                                                  DateOfBirth = s.DateOfBirth,
                                                  EmailId = s.EmailId,
                                                  CellNumber = s.CellNumber,
                                                  Gender = s.Gender,
                                                  ImagePath = s.ImagePath,
                                                  Status = s.Status ?? false


                                              }).ToList();
            return model;
        }
        public StudentInfoViewModel Edit(int studentId)
        {
            StudentInfoViewModel model = new StudentInfoViewModel();
            var data = _ent.StudentInfoes.Where(x => x.StudentId == studentId).FirstOrDefault();
            if (data != null)
            {
                model.StudentId = data.StudentId;
                model.ClassId = data.ClassId ?? 0;
                model.FacultyId = data.FacultyId ?? 0;
                model.IsNew = data.IsNew ?? false;
                model.Reference = data.Reference;
                model.StudentCode = data.StudentCode;
                model.FirstName = data.FirstName;
                model.MidName = data.MidName;
                model.LastName = data.LastName;
                model.StudentType = data.StudentType;
                model.Religion = data.Religion;
                model.IsTransportation = data.IsTransportation ?? false;
                model.PState = data.PState;
                model.PDistrict = data.PDistrict;
                model.PMetropolitan = data.PMetropolitan;
                model.PSubMetropolitan = data.PSubMetropolitan;
                model.PMunicipality = data.PMunicipality;
                model.PGauPalika = data.PGauPalika;
                model.PWardNo = data.PWardNo;
                model.TState = data.TState;
                model.TDistrict = data.TDistrict;
                model.TMetropolitan = data.TMetropolitan;
                model.TSubMetropolitan = data.TSubMetropolitan;
                model.TMunicipality = data.TMunicipality;
                model.TGauPalika = data.TGauPalika;
                model.TWardNo = data.TWardNo;
                model.BloodGroup = data.BloodGroup;
                model.DateOfBirth = data.DateOfBirth;
                model.EmailId = data.EmailId;
                model.CellNumber = data.CellNumber;
                model.Gender = data.Gender;
                model.ImageName = data.ImageName;
                model.ImagePath = data.ImagePath;
                model.Status = data.Status ?? false;
            }
            return model;
        }

        public int Save(StudentInfoViewModel model)
        {
            //return 0:save failed
            //retur 1:save success
            //return 2 already exit

            var studentInfoEntity = new StudentInfo();
            studentInfoEntity.StudentId = model.StudentId;
            studentInfoEntity.ClassId = model.ClassId;
            studentInfoEntity.FacultyId = model.FacultyId;
            studentInfoEntity.Reference = model.Reference;
            studentInfoEntity.StudentCode = model.StudentCode;
            studentInfoEntity.FirstName = model.FirstName;
            studentInfoEntity.MidName = model.MidName;
            studentInfoEntity.LastName = model.LastName;
            studentInfoEntity.StudentType = model.StudentType;
            studentInfoEntity.Religion = model.Religion;
            studentInfoEntity.IsTransportation = model.IsTransportation;
            studentInfoEntity.PState = model.PState;
            studentInfoEntity.PDistrict = model.PDistrict;
            studentInfoEntity.PMetropolitan = model.PMetropolitan;
            studentInfoEntity.PSubMetropolitan = model.PSubMetropolitan;
            studentInfoEntity.PMunicipality = model.PMunicipality;
            studentInfoEntity.PGauPalika = model.PGauPalika;
            studentInfoEntity.PWardNo = model.PWardNo;
            studentInfoEntity.TState = model.TState;
            studentInfoEntity.TDistrict = model.TDistrict;
            studentInfoEntity.TMetropolitan = model.TMetropolitan;
            studentInfoEntity.TSubMetropolitan = model.TSubMetropolitan;
            studentInfoEntity.TMunicipality = model.TMunicipality;
            studentInfoEntity.TGauPalika = model.TGauPalika;
            studentInfoEntity.TWardNo = model.TWardNo;
            studentInfoEntity.BloodGroup = model.BloodGroup;
            studentInfoEntity.DateOfBirth = model.DateOfBirth;
            studentInfoEntity.EmailId = model.EmailId;
            studentInfoEntity.CellNumber = model.CellNumber;
            studentInfoEntity.Gender = model.Gender;
            studentInfoEntity.ImageName = model.ImageName;
            studentInfoEntity.ImagePath = model.ImagePath;

            if (model.StudentId == 0)
            {
                var data = _ent.StudentInfoes.Where(x => x.StudentCode == model.StudentCode.Trim() && x.DeletedDate == null && x.CellNumber == model.CellNumber && x.EmailId == model.EmailId).ToList();
                if (data.Count > 0)
                {
                    return 2;
                }
            }
            var user = Userprovider.CustomeUserManager.GetUserDetails();
            if (model.StudentId > 0)
            {
                studentInfoEntity.Status = model.Status;
                studentInfoEntity.IsNew = model.IsNew;
                studentInfoEntity.UpdatedDate = DateTime.UtcNow;
                studentInfoEntity.UpdatedBy = user.UserId;
                _ent.Entry(studentInfoEntity).State = EntityState.Modified;
                _ent.Entry(studentInfoEntity).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(studentInfoEntity).Property(x => x.CreatedDate).IsModified = false;
            }
            else
            {
                studentInfoEntity.Status = true;
                studentInfoEntity.IsNew = true;
                studentInfoEntity.CreatedDate = DateTime.UtcNow;
                studentInfoEntity.CreatedBy = user.UserId;
                _ent.Entry(studentInfoEntity).State = EntityState.Added;
            }
            try
            {
                _ent.SaveChanges();
                return 1;
            }
            catch (Exception ex)
            {

                return 0;
            }
        }

        public int Delete(int studentId)
        {
            var data = _ent.StudentInfoes.Where(x => x.StudentId == studentId).FirstOrDefault();
            if (data != null)
            {
                var user = Userprovider.CustomeUserManager.GetUserDetails();
                data.DeletedBy = user.UserId;
                data.DeletedDate = DateTime.UtcNow;
                _ent.Entry(data).State = EntityState.Modified;
                try
                {
                    _ent.SaveChanges();
                    return 1;
                }
                catch (Exception ex)
                {

                    return 0;
                }

            }
            return 0;
        }

        public bool InitRegistration(StudentInfoViewModel model)
        {
            //Student Registration Start
            //---------------------<--->----------------------------
            var studentInfoEntity = new StudentInfo();

            studentInfoEntity.StudentId = model.StudentId;
            studentInfoEntity.ClassId = model.ClassId;
            studentInfoEntity.FacultyId = model.FacultyId;
            studentInfoEntity.StudentCode = model.StudentCode;
            studentInfoEntity.FirstName = model.FirstName;
            studentInfoEntity.MidName = model.MidName;
            studentInfoEntity.LastName = model.LastName;
            studentInfoEntity.PState = model.PState;
            studentInfoEntity.PDistrict = model.PDistrict;
            studentInfoEntity.PMetropolitan = model.PMetropolitan;
            studentInfoEntity.PSubMetropolitan = model.PSubMetropolitan;
            studentInfoEntity.PMunicipality = model.PMunicipality;
            studentInfoEntity.PGauPalika = model.PGauPalika;
            studentInfoEntity.PWardNo = model.PWardNo;
            studentInfoEntity.TState = model.TState;
            studentInfoEntity.TDistrict = model.TDistrict;
            studentInfoEntity.TMetropolitan = model.TMetropolitan;
            studentInfoEntity.TSubMetropolitan = model.TSubMetropolitan;
            studentInfoEntity.TMunicipality = model.TMunicipality;
            studentInfoEntity.TGauPalika = model.TGauPalika;
            studentInfoEntity.TWardNo = model.TWardNo;
            studentInfoEntity.BloodGroup = model.BloodGroup;
            studentInfoEntity.DateOfBirth = model.DateOfBirth;
            studentInfoEntity.EmailId = model.EmailId;
            studentInfoEntity.CellNumber = model.CellNumber;
            studentInfoEntity.Gender = model.Gender;
            studentInfoEntity.ImageName = model.ImageName;
            studentInfoEntity.ImagePath = model.ImagePath;
            studentInfoEntity.Reference = model.Reference;
            studentInfoEntity.IsTransportation = model.IsTransportation;

            //Student Registration End
            //---------------------<--->----------------------------





            if (model.StudentId > 0)
            {
                studentInfoEntity.Status = model.Status;
                studentInfoEntity.UpdatedDate = DateTime.UtcNow;
                studentInfoEntity.UpdatedBy = 1;
                _ent.Entry(studentInfoEntity).State = EntityState.Modified;
                _ent.Entry(studentInfoEntity).State = EntityState.Modified;
                _ent.Entry(studentInfoEntity).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(studentInfoEntity).Property(x => x.CreatedDate).IsModified = false;
            }
            else
            {
                studentInfoEntity.Status = true;
                studentInfoEntity.IsNew = true;
                studentInfoEntity.CreatedDate = DateTime.UtcNow;
                studentInfoEntity.CreatedBy = 1;
                _ent.Entry(studentInfoEntity).State = EntityState.Added;
                _ent.SaveChanges();




                //Parent Registration Start
                //---------------------<--->----------------------------

                //var parentInfoEntity = new ParentInfo();
                //parentInfoEntity.StudentId = studentInfoEntity.StudentId;
                //parentInfoEntity.FirstName = model.ObjParentInfoViewModel.FirstName;
                //parentInfoEntity.MidName = model.ObjParentInfoViewModel.MidName;
                //parentInfoEntity.LastName = model.ObjParentInfoViewModel.LastName;
                //parentInfoEntity.PState = model.ObjParentInfoViewModel.PState;
                //parentInfoEntity.PDistrict = model.ObjParentInfoViewModel.PDistrict;
                //parentInfoEntity.PMetropolitan = model.ObjParentInfoViewModel.PMetropolitan;
                //parentInfoEntity.PSubMetropolitan = model.ObjParentInfoViewModel.PSubMetropolitan;
                //parentInfoEntity.PMunicipality = model.ObjParentInfoViewModel.PMunicipality;
                //parentInfoEntity.PGauPalika = model.ObjParentInfoViewModel.PGauPalika;
                //parentInfoEntity.PWardNo = model.ObjParentInfoViewModel.PWardNo;
                //parentInfoEntity.TState = model.ObjParentInfoViewModel.TState;
                //parentInfoEntity.TDistrict = model.ObjParentInfoViewModel.TDistrict;
                //parentInfoEntity.TMetropolitan = model.ObjParentInfoViewModel.TMetropolitan;
                //parentInfoEntity.TSubMetropolitan = model.ObjParentInfoViewModel.TSubMetropolitan;
                //parentInfoEntity.TMunicipality = model.ObjParentInfoViewModel.TMunicipality;
                //parentInfoEntity.TGauPalika = model.ObjParentInfoViewModel.TGauPalika;
                //parentInfoEntity.TWardNo = model.ObjParentInfoViewModel.TWardNo;
                //parentInfoEntity.BloodGroup = model.ObjParentInfoViewModel.BloodGroup;
                //parentInfoEntity.Occupation = model.ObjParentInfoViewModel.Occupation;
                //parentInfoEntity.Gender = model.ObjParentInfoViewModel.Gender;
                //parentInfoEntity.EmailId = model.ObjParentInfoViewModel.EmailId;
                //parentInfoEntity.CellNumber = model.ObjParentInfoViewModel.CellNumber;
                //parentInfoEntity.PhoneNumber = model.ObjParentInfoViewModel.PhoneNumber;
                //parentInfoEntity.CitizenNumber = model.ObjParentInfoViewModel.CitizenNumber;

                //_ent.Entry(parentInfoEntity).State = EntityState.Added;

                ////---------------------<--->----------------------------
                ////Parent Registration End


                ////Guardian Registration Start
                ////---------------------<--->----------------------------

                //var guardianInfoEntity = new GuardianInfo();
                //guardianInfoEntity.StudentId = studentInfoEntity.StudentId;
                //guardianInfoEntity.FirstName = model.ObjGuardianInfoViewModel.FirstName;
                //guardianInfoEntity.MidName = model.ObjGuardianInfoViewModel.MidName;
                //guardianInfoEntity.LastName = model.ObjGuardianInfoViewModel.LastName;
                //guardianInfoEntity.PState = model.ObjGuardianInfoViewModel.PState;
                //guardianInfoEntity.PDistrict = model.ObjGuardianInfoViewModel.PDistrict;
                //guardianInfoEntity.PMetropolitan = model.ObjGuardianInfoViewModel.PMetropolitan;
                //guardianInfoEntity.PSubMetropolitan = model.ObjGuardianInfoViewModel.PSubMetropolitan;
                //guardianInfoEntity.PMunicipality = model.ObjGuardianInfoViewModel.PMunicipality;
                //guardianInfoEntity.PGauPalika = model.ObjGuardianInfoViewModel.PGauPalika;
                //guardianInfoEntity.PWardNo = model.ObjGuardianInfoViewModel.PWardNo;
                //guardianInfoEntity.TState = model.ObjGuardianInfoViewModel.TState;
                //guardianInfoEntity.TDistrict = model.ObjGuardianInfoViewModel.TDistrict;
                //guardianInfoEntity.TMetropolitan = model.ObjGuardianInfoViewModel.TMetropolitan;
                //guardianInfoEntity.TSubMetropolitan = model.ObjGuardianInfoViewModel.TSubMetropolitan;
                //guardianInfoEntity.TMunicipality = model.ObjGuardianInfoViewModel.TMunicipality;
                //guardianInfoEntity.TGauPalika = model.ObjGuardianInfoViewModel.TGauPalika;
                //guardianInfoEntity.TWardNo = model.ObjGuardianInfoViewModel.TWardNo;
                //guardianInfoEntity.BloodGroup = model.ObjGuardianInfoViewModel.BloodGroup;
                //guardianInfoEntity.Occupation = model.ObjGuardianInfoViewModel.Occupation;
                //guardianInfoEntity.Gender = model.ObjGuardianInfoViewModel.Gender;
                //guardianInfoEntity.EmailId = model.ObjGuardianInfoViewModel.EmailId;
                //guardianInfoEntity.CellNumber = model.ObjGuardianInfoViewModel.CellNumber;
                //guardianInfoEntity.PhoneNumber = model.ObjGuardianInfoViewModel.PhoneNumber;
                //guardianInfoEntity.CitizenNumber = model.ObjGuardianInfoViewModel.CitizenNumber;

                //_ent.Entry(guardianInfoEntity).State = EntityState.Added;

                //---------------------<--->----------------------------
                //Guardian Registration End


                var data = (from c in _ent.ClassSubjects
                            join s in _ent.SetupSubjects on c.SubjectId equals s.SubjectId
                            where s.IsOptional == false && c.ClassId==model.ClassId
                            select new
                            {
                                SubjectId = s.SubjectId,
                                ClassId = model.ClassId,
                                FacultyId = model.FacultyId,
                                SectionId = model.SectionId,
                                BlockId =  model.BlockId
                                

                            }).ToList();

                var entStudentSubjectDetails = new StudentSubjectDetail();
                var entStudentClassInfo = new StudentClassInfo();

                foreach (var item in data)
                {
                    entStudentSubjectDetails.SubjectId = item.SubjectId;
                    entStudentSubjectDetails.StudentId = studentInfoEntity.StudentId;

                    entStudentClassInfo.ClassId = item.ClassId;
                    entStudentClassInfo.FacultyId = item.FacultyId??0;
                    entStudentClassInfo.SectionId = item.SectionId;
                    entStudentClassInfo.BlockId = item.BlockId;
                    entStudentSubjectDetails.CreatedDate = DateTime.UtcNow;
                    entStudentSubjectDetails.CreatedBy = 1;
                    entStudentSubjectDetails.Status = true;
                    _ent.Entry(entStudentSubjectDetails).State = EntityState.Added;
                    _ent.Entry(entStudentClassInfo).State = EntityState.Added;
                    _ent.SaveChanges();
                }


                _ent.SaveChanges();

            }


            return true;
        }


    }
}
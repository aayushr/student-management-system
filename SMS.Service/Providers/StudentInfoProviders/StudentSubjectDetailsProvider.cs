﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMS.SMS.Entities;
using SMS.Models.StudentInfoViewModel;
using System.Data.Entity;

namespace SMS.Service.Providers.StudentInfoProviders
{
    public interface IStudentSubjectDetailsProviders
    {
        bool Save(StudentSubjectDetailsViewModel model);
        int Update(StudentSubjectDetailsViewModel model);
        StudentSubjectDetailsViewModel GetStudentSubjectDetailsList();
        StudentSubjectDetailsViewModel Edit(int studentId);
        bool Delete(int studentSubjectId);

    }
    public class StudentSubjectDetailsProvider : IStudentSubjectDetailsProviders
    {
        private SMSEntities _ent;
        public StudentSubjectDetailsProvider()
        {
            this._ent = new SMSEntities();
        }

        public bool Delete(int studentSubjectId)
        {
            var data = _ent.StudentSubjectDetails.Where(x => x.StudentSubjectId == studentSubjectId).FirstOrDefault();
            if (data != null)
            {
                var user = Userprovider.CustomeUserManager.GetUserDetails();
                data.DeletedBy = user.UserId;
                data.DeletedDate = DateTime.UtcNow;
                _ent.Entry(data).State = EntityState.Modified;
                _ent.SaveChanges();
            }
            return true;
        }

        public StudentSubjectDetailsViewModel Edit(int studentId)
        {
            StudentSubjectDetailsViewModel model = new StudentSubjectDetailsViewModel();

      

            model.StudentSubjectDetailsViewModelList = (from s in _ent.SetupSubjects
                                                        from sb in _ent.StudentSubjectDetails
                                                             .Where(sb => s.SubjectId == sb.SubjectId)
                                                             .DefaultIfEmpty() // <== makes join left join
                                                             where s.IsOptional ==true
                                                        select new StudentSubjectDetailsViewModel
                                                        {
                                                            //LastName = last != null ? last.Name : default(string),
                                                            SubjectId =s.SubjectId,
                                                            SubjectName = s.SubjectName,
                                                            StudentId = sb.StudentId,
                                                            //StudentName = st.FirstName + " " + st.LastName,
                                                            IsChecked = sb.IsChecked ?? false

                                                        }).ToList();
            //var data = _ent.StudentSubjectDetails.Where(x => x.StudentSubjectId == studentSubjectId).FirstOrDefault();
            //if (data != null)
            //{
            //    model.StudentSubjectId = data.StudentSubjectId;
            //    model.StudentId = data.StudentId;
            //    model.SubjectId = data.SubjectId??0;
            //    model.Status = data.Status ?? false;
            //}

            return model;
        }

        public StudentSubjectDetailsViewModel GetStudentSubjectDetailsList()
        {
            StudentSubjectDetailsViewModel model = new StudentSubjectDetailsViewModel();
            model.StudentSubjectDetailsViewModelList = (from sd in _ent.StudentSubjectDetails
                                                        join s in _ent.StudentInfoes on sd.StudentId equals s.StudentId
                                                        join su in _ent.SetupSubjects on sd.SubjectId equals su.SubjectId
                                                        where sd.DeletedDate == null 
                                                        select new StudentSubjectDetailsViewModel
                                                        {
                                                            StudentSubjectId = sd.StudentSubjectId,
                                                            StudentName = s.FirstName + " " + s.MidName + " " + s.LastName,
                                                            SubjectName = su.SubjectName,
                                                            Status = sd.Status ?? false,
                                                            StudentId = sd.StudentId
                                                            

                                                        }).ToList();

            return model;

        }

        public bool Save(StudentSubjectDetailsViewModel model)
        {
            var user = Userprovider.CustomeUserManager.GetUserDetails();

            foreach (var item in model.SetupSubjectViewModelList)
            {
                StudentSubjectDetail studentSubjectDetailEntity = new StudentSubjectDetail();
                studentSubjectDetailEntity.StudentId = model.StudentId;
                studentSubjectDetailEntity.SubjectId = item.SubjectId;
                studentSubjectDetailEntity.Status = true;
                studentSubjectDetailEntity.IsChecked = true;
                studentSubjectDetailEntity.CreatedBy =user.UserId;
                studentSubjectDetailEntity.CreatedDate = DateTime.UtcNow;
                _ent.Entry(studentSubjectDetailEntity).State = EntityState.Added;

                _ent.SaveChanges();

            }

            return true;
        }
        public int Update(StudentSubjectDetailsViewModel model)
        {

            var user = Userprovider.CustomeUserManager.GetUserDetails();
            var data = (from su in _ent.StudentSubjectDetails
                        join s in _ent.SetupSubjects
                        on su.SubjectId equals s.SubjectId
                        where su.StudentId == model.StudentId && s.IsOptional == true
                        select new
                        {
                            SubjectId = su.SubjectId,
                            StudentSubjectId = su.StudentSubjectId,
                            StudentId = su.StudentId,

                        }).ToList();


            foreach (var items in data)
            {
                StudentSubjectDetail studentSubjectDetailEntity = new StudentSubjectDetail();
                studentSubjectDetailEntity.StudentSubjectId = items.StudentSubjectId;
                studentSubjectDetailEntity.SubjectId = items.SubjectId;
                studentSubjectDetailEntity.Status = model.Status;
                studentSubjectDetailEntity.StudentId = items.SubjectId;
                _ent.Entry(studentSubjectDetailEntity).State = EntityState.Deleted;
                _ent.SaveChanges();
            }

            foreach (var item in model.SetupSubjectViewModelList)
            {
                StudentSubjectDetail studentSubjectDetailEntity = new StudentSubjectDetail();
                studentSubjectDetailEntity.StudentId = model.StudentId;
                studentSubjectDetailEntity.SubjectId = item.SubjectId;
                studentSubjectDetailEntity.Status = true;
                studentSubjectDetailEntity.IsChecked = true;
                studentSubjectDetailEntity.UpdatedBy = user.UserId;
                studentSubjectDetailEntity.UpdatedDate = DateTime.UtcNow;
                _ent.Entry(studentSubjectDetailEntity).State = EntityState.Added;

                _ent.SaveChanges();

            }

            _ent.SaveChanges();

            return 1;
        }

    }
}

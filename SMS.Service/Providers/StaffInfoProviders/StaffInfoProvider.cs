﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMS.SMS.Entities;
using SMS.Models.StaffInfoViewModel;
using System.Data.Entity;

namespace SMS.Service.Providers.StaffInfoProvider
{

    public interface IStaffInfoProvider
    {
        int Save(StaffInfoViewModel model);
        StaffInfoViewModel Edit(int staffId);
        StaffInfoViewModel GetStaffList();
        int Delete(int staffId);
    }
    public class StaffInfoProvider : IStaffInfoProvider
    {
        public readonly SMSEntities _ent;
        public StaffInfoProvider()
        {
            this._ent = new SMSEntities();
        }

        public int Delete(int staffId)
        {
            var data = _ent.StaffInfoes.Where(x => x.StaffId == staffId).FirstOrDefault();
            if (data != null)
            {
                var user = Userprovider.CustomeUserManager.GetUserDetails();
                data.DeletedBy = user.UserId;
                data.DeletedDate = DateTime.UtcNow;
                _ent.Entry(data).State = EntityState.Modified;
                _ent.SaveChanges();
                try
                {
                    return 1;
                }
                catch (Exception ee)
                {

                    return 0;
                }
            }
            return 0;
        }

        public StaffInfoViewModel Edit(int staffId)
        {
            StaffInfoViewModel model = new StaffInfoViewModel();
            var data = _ent.StaffInfoes.Where(x => x.StaffId == staffId).FirstOrDefault();
            if (data != null)
            {
                model.StaffId = data.StaffId;
                model.ServiceId = data.ServiceId;
                model.PostId = data.PostId;
                model.DepartmentId = data.DepartmentId;
                model.FirstName = data.FirstName;
                model.MidName = data.MidName;
                model.LastName = data.LastName;
                model.PState = data.PState;
                model.PDistrict = data.PDistrict;
                model.PMetropolitan = data.PMetropolitan;
                model.PSubMetropolitan = data.PSubMetropolitan;
                model.PMunicipality = data.PMunicipality;
                model.PGauPalika = data.PGauPalika;
                model.PWardNo = data.PWardNo;
                model.TState = data.TState;
                model.TDistrict = data.TDistrict;
                model.TMetropolitan = data.TMetropolitan;
                model.TSubMetropolitan = data.TSubMetropolitan;
                model.TMunicipality = data.TMunicipality;
                model.TGauPalika = data.TGauPalika;
                model.TWardNo = data.TWardNo;
                model.BloodGroup = data.BloodGroup;
                model.Occupation = data.Occupation;
                model.Gender = data.Gender;
                model.EmailId = data.EmailId;
                model.CellNumber = data.CellNumber;
                model.PhoneNumber = data.PhoneNumber;
                model.CitizenNumber = data.CitizenNumber;
                model.Status = data.Status ?? false;
            }
            return model;
        }

        public StaffInfoViewModel GetStaffList()
        {
            StaffInfoViewModel model = new StaffInfoViewModel();
            model.StaffInfoViewModelList = (from s in _ent.StaffInfoes
                                            where s.DeletedDate == null
                                            select new StaffInfoViewModel
                                            {
                                                StaffId = s.StaffId,
                                                ServiceId = s.ServiceId,
                                                PostId = s.PostId,
                                                DepartmentId = s.DepartmentId,
                                                FirstName = s.FirstName,
                                                MidName = s.MidName,
                                                LastName = s.LastName,
                                                PState = s.PState,
                                                PDistrict = s.PDistrict,
                                                PMetropolitan = s.PMetropolitan,
                                                PSubMetropolitan = s.PSubMetropolitan,
                                                PMunicipality = s.PMunicipality,
                                                PGauPalika = s.PGauPalika,
                                                PWardNo = s.PWardNo,
                                                TState = s.TState,
                                                TDistrict = s.TDistrict,
                                                TMetropolitan = s.TMetropolitan,
                                                TSubMetropolitan = s.TSubMetropolitan,
                                                TMunicipality = s.TMunicipality,
                                                TGauPalika = s.TGauPalika,
                                                TWardNo = s.TWardNo,
                                                BloodGroup = s.BloodGroup,
                                                Occupation = s.Occupation,
                                                Gender = s.Gender,
                                                EmailId = s.EmailId,
                                                CellNumber = s.CellNumber,
                                                PhoneNumber = s.PhoneNumber,
                                                CitizenNumber = s.CitizenNumber,
                                                Status = s.Status ?? false
                                            }).ToList();
            return model;
        }

        public int Save(StaffInfoViewModel model)
        {
            //return 0:save failed
            //retur 1:save success
            //return 2 already exit
            StaffInfo StaffInfoEntity = new StaffInfo();
            var data = _ent.StaffInfoes.Where(x => x.FirstName == model.FirstName.Trim()).ToList();
            if (data.Count > 0)
            {
                return 2;
            }
            var user = Userprovider.CustomeUserManager.GetUserDetails();
            StaffInfoEntity.ServiceId = model.ServiceId;
            StaffInfoEntity.PostId = model.PostId;
            StaffInfoEntity.DepartmentId = model.DepartmentId;
            StaffInfoEntity.FirstName = model.FirstName;
            StaffInfoEntity.MidName = model.MidName;
            StaffInfoEntity.LastName = model.LastName;
            StaffInfoEntity.PState = model.PState;
            StaffInfoEntity.PDistrict = model.PDistrict;
            StaffInfoEntity.PMetropolitan = model.PMetropolitan;
            StaffInfoEntity.PSubMetropolitan = model.PSubMetropolitan;
            StaffInfoEntity.PMunicipality = model.PMunicipality;
            StaffInfoEntity.PGauPalika = model.PGauPalika;
            StaffInfoEntity.PWardNo = model.PWardNo;
            StaffInfoEntity.TState = model.TState;
            StaffInfoEntity.TDistrict = model.TDistrict;
            StaffInfoEntity.TMetropolitan = model.TMetropolitan;
            StaffInfoEntity.TSubMetropolitan = model.TSubMetropolitan;
            StaffInfoEntity.TMunicipality = model.TMunicipality;
            StaffInfoEntity.TGauPalika = model.TGauPalika;
            StaffInfoEntity.TWardNo = model.TWardNo;
            StaffInfoEntity.BloodGroup = model.BloodGroup;
            StaffInfoEntity.Occupation = model.Occupation;
            StaffInfoEntity.Gender = model.Gender;
            StaffInfoEntity.EmailId = model.EmailId;
            StaffInfoEntity.CellNumber = model.CellNumber;
            StaffInfoEntity.PhoneNumber = model.PhoneNumber;
            StaffInfoEntity.CitizenNumber = model.CitizenNumber;

            if (model.StaffId > 0)
            {
                StaffInfoEntity.StaffId = model.StaffId;
                StaffInfoEntity.UpdatedBy = user.UserId;
                StaffInfoEntity.Status = model.Status;
                StaffInfoEntity.UpdatedDate = DateTime.UtcNow;
                _ent.Entry(StaffInfoEntity).State = EntityState.Modified;
                _ent.Entry(StaffInfoEntity).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(StaffInfoEntity).Property(x => x.CreatedDate).IsModified = false;

            }
            else
            {
                StaffInfoEntity.Status = true;
                StaffInfoEntity.CreatedBy = user.UserId;
                StaffInfoEntity.CreatedDate = DateTime.UtcNow;
                _ent.Entry(StaffInfoEntity).State = EntityState.Added;
            }
            try
            {
                _ent.SaveChanges();
                return 1;
            }
            catch (Exception ee)
            {
                return 0;

            }


        }
    }
}

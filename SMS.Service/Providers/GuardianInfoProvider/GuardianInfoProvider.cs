﻿using System;
using System.Linq;
using SMS.Models.GuardianInfoViewModel;
using System.Data.Entity;
using SMS.SMS.Entities;
using SMS.Service.Providers.Userprovider;

namespace SMS.Services.Providers
{
    public interface IGuardianInfoProvider
    {
        GuardianInfoViewModel GetGuardianList();
        bool Save(GuardianInfoViewModel model);
        GuardianInfoViewModel Edit(int guardianId);
        bool Delete(int guardianId);

    }
    public class GuardianInfoProvider : IGuardianInfoProvider
    {
        private readonly SMSEntities _ent;
        public GuardianInfoProvider()
        {
            _ent = new SMSEntities();
        }
        public bool Delete(int id)
        {

            var data = _ent.GuardianInfoes.Where(x => x.GuardianId == id).FirstOrDefault();
            if (data != null)
            {

                data.DeletedBy = 1;
                data.DeletedDate = DateTime.UtcNow;
                _ent.Entry(data).State = EntityState.Modified;
                _ent.SaveChanges();


            }
            return true;
        }

        public GuardianInfoViewModel Edit(int id)
        {
            GuardianInfoViewModel model = new GuardianInfoViewModel();
            var data = _ent.GuardianInfoes.Where(x => x.GuardianId == id).SingleOrDefault();
            if (data != null)
            {
                model.GuardianId = data.GuardianId;
                model.StudentId = 1;
                model.FirstName = data.FirstName;
                model.MidName = data.MidName;
                model.LastName = data.LastName;
                model.PState = data.PState;
                model.PDistrict = data.PDistrict;
                model.PMetropolitan = data.PMetropolitan;
                model.PSubMetropolitan = data.PSubMetropolitan;
                model.PMunicipality = data.PMunicipality;
                model.PGauPalika = data.PGauPalika;
                model.PWardNo = data.PWardNo;
                model.TState = data.TState;
                model.TDistrict = data.TDistrict;
                model.TMunicipality = data.TMunicipality;
                model.TSubMetropolitan = data.TSubMetropolitan;
                model.TMetropolitan = data.TMetropolitan;
                model.TGauPalika = data.TGauPalika;
                model.TWardNo = data.TWardNo;
                model.BloodGroup = data.BloodGroup;
                model.Occupation = data.Occupation;
                model.Gender = data.Gender;
                model.EmailId = data.EmailId;
                model.CellNumber = data.CellNumber;
                model.PhoneNumber = data.PhoneNumber;
                model.CitizenNumber = data.CitizenNumber;
                model.Status = data.Status ?? false;
                model.UpdatedBy = 1;
                model.UpdatedDate = DateTime.UtcNow;

            }
            return model;
        }

        public GuardianInfoViewModel GetGuardianList()
        {
            GuardianInfoViewModel model = new GuardianInfoViewModel();
            model.GuardianInfoViewModelList = (from s in _ent.GuardianInfoes
                                               where s.DeletedDate == null
                                               select new GuardianInfoViewModel
                                               {
                                                   GuardianId = s.GuardianId,
                                                   StudentId = s.StudentId,
                                                   FirstName = s.FirstName,
                                                   MidName = s.MidName,
                                                   LastName = s.LastName,
                                                   PState = s.PState,
                                                   PDistrict = s.PDistrict,
                                                   PMetropolitan = s.PMetropolitan,
                                                   PSubMetropolitan = s.PSubMetropolitan,
                                                   PMunicipality = s.PMunicipality,
                                                   PGauPalika = s.PGauPalika,
                                                   PWardNo = s.PWardNo,
                                                   TDistrict = s.TDistrict,
                                                   TState = s.TState,
                                                   TMetropolitan = s.TMetropolitan,
                                                   TSubMetropolitan = s.TSubMetropolitan,
                                                   TMunicipality = s.TMunicipality,
                                                   TGauPalika = s.TGauPalika,
                                                   TWardNo = s.TWardNo,
                                                   BloodGroup = s.BloodGroup,
                                                   Occupation = s.Occupation,
                                                   Gender = s.Gender,
                                                   EmailId = s.EmailId,
                                                   CellNumber = s.CellNumber,
                                                   PhoneNumber = s.PhoneNumber,
                                                   CitizenNumber = s.CitizenNumber,
                                                   Status = s.Status ?? false,

                                               }).ToList();
            return model;
        }

        public bool Save(GuardianInfoViewModel model)
        {

            GuardianInfo entittyGuardianInfo = new GuardianInfo();
            entittyGuardianInfo.StudentId = model.StudentId;
            entittyGuardianInfo.FirstName = model.FirstName;
            entittyGuardianInfo.MidName = model.MidName;
            entittyGuardianInfo.LastName = model.LastName;
            entittyGuardianInfo.PState = model.PState;
            entittyGuardianInfo.PDistrict = model.PDistrict;
            entittyGuardianInfo.PMetropolitan = model.PMetropolitan;
            entittyGuardianInfo.PSubMetropolitan = model.PSubMetropolitan;
            entittyGuardianInfo.PMunicipality = model.PMunicipality;
            entittyGuardianInfo.PGauPalika = model.PGauPalika;
            entittyGuardianInfo.PWardNo = model.PWardNo;
            entittyGuardianInfo.TState = model.TState;
            entittyGuardianInfo.TDistrict = model.TDistrict;
            entittyGuardianInfo.TMetropolitan = model.TMetropolitan;
            entittyGuardianInfo.TSubMetropolitan = model.TSubMetropolitan;
            entittyGuardianInfo.TMunicipality = model.TMunicipality;
            entittyGuardianInfo.TGauPalika = model.TGauPalika;
            entittyGuardianInfo.TWardNo = model.TWardNo;
            entittyGuardianInfo.BloodGroup = model.BloodGroup;
            entittyGuardianInfo.Occupation = model.Occupation;
            entittyGuardianInfo.Gender = model.Gender;
            entittyGuardianInfo.EmailId = model.EmailId;
            entittyGuardianInfo.CellNumber = model.CellNumber;
            entittyGuardianInfo.PhoneNumber = model.PhoneNumber;
            entittyGuardianInfo.CitizenNumber = model.CitizenNumber;
            entittyGuardianInfo.GuardianId = model.GuardianId;
            entittyGuardianInfo.Status = model.Status;
            if (model.GuardianId > 0)
            {

                entittyGuardianInfo.UpdatedBy = 1;
                entittyGuardianInfo.UpdatedDate = DateTime.UtcNow;
                _ent.Entry(entittyGuardianInfo).State = EntityState.Modified;
                _ent.Entry(entittyGuardianInfo).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(entittyGuardianInfo).Property(x => x.CreatedDate).IsModified = false;

            }
            else
            {
                entittyGuardianInfo.Status = true;
                entittyGuardianInfo.CreatedBy = 1;
                entittyGuardianInfo.CreatedDate = DateTime.UtcNow;
                _ent.Entry(entittyGuardianInfo).State = EntityState.Added;
            }

            _ent.SaveChanges();
            return true;


        }
    }
}

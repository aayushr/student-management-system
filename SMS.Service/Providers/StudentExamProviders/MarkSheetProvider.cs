﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMS.Models.MarkSheetViewModel;
using SMS.SMS.Entities;
using System.Data.Entity;




namespace SMS.Service.Providers.MarkSheetProviders
{
    public interface IMarkSheetProvider
    {
        int Save(MarkSheetViewModel model);
        MarkSheetViewModel Edit(int markSheetId);
        int Delete(int markSheetId);
        MarkSheetViewModel GetMarkSheetList();
    }
    public class MarkSheetProvider : IMarkSheetProvider
    {
        private readonly SMSEntities _ent;
        public MarkSheetProvider()
        {
            this._ent = new SMSEntities();
        }

        public int Delete(int markSheetId)
        {
            var data = _ent.MarkSheets.Where(x => x.MarkSheetId == markSheetId).FirstOrDefault();
            if (data != null)
            {
                data.DeletedBy = 1;
                data.DeletedDate = DateTime.UtcNow;
                _ent.Entry(data).State = EntityState.Modified;
                _ent.SaveChanges();
                try
                {
                    return 1;
                }
                catch (Exception e)
                {

                    return 0;
                }
            }
            return 0;
        }

        public MarkSheetViewModel Edit(int markSheetId)
        {
            MarkSheetViewModel model = new MarkSheetViewModel();
            var data = _ent.MarkSheets.Where(x => x.MarkSheetId == markSheetId).FirstOrDefault();
            if (data != null)
            {
                model.MarkSheetId = data.MarkSheetId;
                model.TerminalExamId = data.TerminalExamId;
                model.StudentId = data.StudentId;
                model.ClassId = data.ClassId;
                model.FacultyId = data.FacultyId;
                model.SectionId = data.SectionId;
                model.SubjectId = data.SubjectId;
                model.ObtainThroryMarks = data.ObtainThroryMarks;
                model.ObtainPracticalMarks = data.ObtainPracticalMarks;
                model.ObtainTotalMarks = data.ObtainTotalMarks;
                model.Remarks = data.Remarks;
                model.Status = data.Status??false;
            }
            return model;
        }

        public MarkSheetViewModel GetMarkSheetList()
        {
            MarkSheetViewModel model = new MarkSheetViewModel();
            model.MarkSheetViewModelList = (from m in _ent.MarkSheets
                                            join t in _ent.SetupTerminalExams on m.TerminalExamId equals t.TerminalExamId
                                            join s in _ent.StudentInfoes on m.StudentId equals s.StudentId
                                            join c in _ent.SetupClasses on m.ClassId equals c.ClassId
                                            join f in _ent.SetupFaculties on m.FacultyId equals f.FacultyId
                                            join sec in _ent.SetupSections on m.SectionId equals sec.SectionId
                                            join sub in _ent.SetupSubjects on m.SubjectId equals sub.SubjectId
                                            where m.DeletedDate == null
                                            select new MarkSheetViewModel
                                            {
                                                MarkSheetId = m.MarkSheetId,
                                                TerminalExamName = t.Name,
                                                TerminalExamId=m.TerminalExamId,
                                                StudentName = s.FirstName + s.LastName,
                                                StudentId=m.StudentId,
                                                ClassName = c.ClassName,
                                                ClassId=m.ClassId,
                                                FacultyName = f.FacultyName,
                                                FacultyId=m.FacultyId,
                                                SectionName = sec.SectionName,
                                                SectionId=m.SectionId,
                                                SubjectName = sub.SubjectName,
                                                SubjectId=m.SubjectId,
                                                ObtainThroryMarks = m.ObtainThroryMarks,
                                                ObtainPracticalMarks = m.ObtainPracticalMarks,
                                                ObtainTotalMarks = m.ObtainTotalMarks,
                                                Remarks = m.Remarks,
                                                Status = m.Status??false
                                            }).ToList();
            return model;
        }


        public int Save(MarkSheetViewModel model)
        {
            if (model.MarkSheetId == 0)
            {
                var data = _ent.MarkSheets.Where(x => x.TerminalExamId == model.TerminalExamId && x.StudentId == model.StudentId && x.ClassId == model.ClassId && x.FacultyId == model.FacultyId && x.SectionId == model.SectionId && x.SubjectId == model.SubjectId).ToList();
                if (data.Count > 0)
                {
                    return 2;
                }
            }
            MarkSheet markSheetEntity = new MarkSheet();
            markSheetEntity.TerminalExamId = model.TerminalExamId;
            markSheetEntity.StudentId = model.StudentId;
            markSheetEntity.ClassId = model.ClassId;
            markSheetEntity.FacultyId = model.FacultyId;
            markSheetEntity.SectionId = model.SectionId;
            markSheetEntity.SubjectId = model.SubjectId;
            markSheetEntity.ObtainThroryMarks = model.ObtainThroryMarks;
            markSheetEntity.ObtainPracticalMarks = model.ObtainPracticalMarks;
            markSheetEntity.ObtainTotalMarks = model.ObtainThroryMarks + model.ObtainPracticalMarks;
            markSheetEntity.Remarks = model.Remarks;
            if (model.MarkSheetId > 0)
            {
                markSheetEntity.MarkSheetId = model.MarkSheetId;
                markSheetEntity.Status = model.Status;
                markSheetEntity.UpdatedBy = 1;
                markSheetEntity.UpdatedDate = DateTime.UtcNow;
                _ent.Entry(markSheetEntity).State = EntityState.Modified;
                _ent.Entry(markSheetEntity).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(markSheetEntity).Property(x => x.CreatedDate).IsModified = false;

            }
            else
            {
                markSheetEntity.Status = true;
                markSheetEntity.CreatedBy = 1;
                markSheetEntity.CreatedDate = DateTime.UtcNow;
                _ent.Entry(markSheetEntity).State = EntityState.Added;
            }
            try
            {
                _ent.SaveChanges();
                return 1;
            }
            catch (Exception e)
            {

                return 0;
            }
        }
    }
}

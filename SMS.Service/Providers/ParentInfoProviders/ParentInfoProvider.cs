﻿using System;
using System.Linq;
using SMS.Models;
using SMS.SMS.Entities;
using System.Data.Entity;
using SMS.Models.ParentInfoViewModel;

namespace SMS.Service.Providers.ParentInfoProviders
{
    //comment
    public interface IParentInfoProvider
    {
        int Save(ParentInfoViewModel model);
        ParentInfoViewModel Edit(int parentId);
        ParentInfoViewModel GetParentList();
        int Delete(int parentId);

    }
    public class ParentInfoProvider : IParentInfoProvider
    {
        private readonly SMSEntities _ent;
        public ParentInfoProvider()
        {
            this._ent = new SMSEntities();

        }

        public int Delete(int parentId)
        {
            var data = _ent.ParentInfoes.Where(model => model.ParentId == parentId).FirstOrDefault();
            if (data != null)
            {
                var user = Userprovider.CustomeUserManager.GetUserDetails();
                data.DeletedBy = user.UserId;
                data.DeletedDate = DateTime.UtcNow;
                _ent.Entry(data).State = EntityState.Modified;
                _ent.SaveChanges();
                try
                {
                    return 1;
                }
                catch (Exception ee)
                {
                    return 0;

                }
            }
            return 0;
        }

        public ParentInfoViewModel Edit(int parentId)
        {
            ParentInfoViewModel model = new ParentInfoViewModel();
            var data = _ent.ParentInfoes.Where(x => x.ParentId == parentId).FirstOrDefault();
            if (data != null)
            {
                model.ParentId = data.ParentId;
                model.StudentId =data.StudentId;
                model.FirstName = data.FirstName;
                model.MidName = data.MidName;
                model.LastName = data.LastName;
                model.PState = data.PState;
                model.PDistrict = data.PDistrict;
                model.PMetropolitan = data.PMetropolitan;
                model.PSubMetropolitan = data.PSubMetropolitan;
                model.PMunicipality = data.PMunicipality;
                model.PGauPalika = data.PGauPalika;
                model.PWardNo = data.PWardNo;
                model.TState = data.TState;
                model.TDistrict = data.TDistrict;
                model.TMetropolitan = data.TMetropolitan;
                model.TSubMetropolitan = data.TSubMetropolitan;
                model.TMunicipality = data.TMunicipality;
                model.TGauPalika = data.TGauPalika;
                model.TWardNo = data.TWardNo;
                model.BloodGroup = data.BloodGroup;
                model.Occupation = data.Occupation;
                model.Gender = data.Gender;
                model.EmailId = data.EmailId;
                model.CellNumber = data.CellNumber;
                model.PhoneNumber = data.PhoneNumber;
                model.CitizenNumber = data.CitizenNumber;
                model.Status = data.Status ?? false;

            }
            return model;
        }

        public ParentInfoViewModel GetParentList()
        {
            ParentInfoViewModel model = new ParentInfoViewModel();
            model.ParentInfoViewModelList = (from s in _ent.ParentInfoes
                                             where s.DeletedDate == null
                                             select new ParentInfoViewModel
                                             {
                                                 ParentId = s.ParentId,
                                                 FirstName = s.FirstName,
                                                 MidName = s.MidName,
                                                 LastName = s.LastName,
                                                 PState = s.PState,
                                                 PDistrict = s.PDistrict,
                                                 PMetropolitan = s.PMetropolitan,
                                                 PSubMetropolitan = s.PSubMetropolitan,
                                                 PMunicipality = s.PMunicipality,
                                                 PGauPalika = s.PGauPalika,
                                                 PWardNo = s.PWardNo,
                                                 TState = s.TState,
                                                 TDistrict = s.TDistrict,
                                                 TMetropolitan = s.TMetropolitan,
                                                 TSubMetropolitan = s.TSubMetropolitan,
                                                 TMunicipality = s.TMunicipality,
                                                 TGauPalika = s.TGauPalika,
                                                 TWardNo = s.TWardNo,
                                                 BloodGroup = s.BloodGroup,
                                                 Occupation = s.Occupation,
                                                 Gender = s.Gender,
                                                 EmailId = s.EmailId,
                                                 CellNumber = s.CellNumber,
                                                 PhoneNumber = s.PhoneNumber,
                                                 CitizenNumber = s.CitizenNumber,
                                                 Status = s.Status ?? false

                                             }).ToList();
            return model;
        }

        public int Save(ParentInfoViewModel model)
        {
            ParentInfo parentInfoEntity = new ParentInfo();

            if (model.ParentId == 0)
            {
                var data = _ent.ParentInfoes.Where(x => x.EmailId == model.EmailId && x.PhoneNumber == model.PhoneNumber).ToList();
                if (data.Count > 0)
                {
                    return 2;
                }
            }
            var user = Userprovider.CustomeUserManager.GetUserDetails();
            parentInfoEntity.StudentId = model.StudentId;
            parentInfoEntity.FirstName = model.FirstName;
            parentInfoEntity.MidName = model.MidName;
            parentInfoEntity.LastName = model.LastName;
            parentInfoEntity.PState = model.PState;
            parentInfoEntity.PDistrict = model.PDistrict;
            parentInfoEntity.PMetropolitan = model.PMetropolitan;
            parentInfoEntity.PSubMetropolitan = model.PSubMetropolitan;
            parentInfoEntity.PMunicipality = model.PMunicipality;
            parentInfoEntity.PGauPalika = model.PGauPalika;
            parentInfoEntity.PWardNo = model.PWardNo;
            parentInfoEntity.TState = model.TState;
            parentInfoEntity.TDistrict = model.TDistrict;
            parentInfoEntity.TMetropolitan = model.TMetropolitan;
            parentInfoEntity.TSubMetropolitan = model.TSubMetropolitan;
            parentInfoEntity.TMunicipality = model.TMunicipality;
            parentInfoEntity.TGauPalika = model.TGauPalika;
            parentInfoEntity.TWardNo = model.TWardNo;
            parentInfoEntity.BloodGroup = model.BloodGroup;
            parentInfoEntity.Occupation = model.Occupation;
            parentInfoEntity.Gender = model.Gender;
            parentInfoEntity.EmailId = model.EmailId;
            parentInfoEntity.CellNumber = model.CellNumber;
            parentInfoEntity.PhoneNumber = model.PhoneNumber;
            parentInfoEntity.CitizenNumber = model.CitizenNumber;

            if (model.ParentId > 0)
            {
                parentInfoEntity.ParentId = model.ParentId;
                parentInfoEntity.UpdatedBy = user.UserId;
                parentInfoEntity.Status = model.Status;
                parentInfoEntity.UpdatedDate = DateTime.UtcNow;
                _ent.Entry(parentInfoEntity).State = EntityState.Modified;
                _ent.Entry(parentInfoEntity).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(parentInfoEntity).Property(x => x.CreatedDate).IsModified = false;

            }
            else
            {
                parentInfoEntity.Status = true;
                parentInfoEntity.CreatedBy = user.UserId;
                parentInfoEntity.CreatedDate = DateTime.UtcNow;
                _ent.Entry(parentInfoEntity).State = EntityState.Added;
            }
            try
            {
                _ent.SaveChanges();
                return 1;
            }
            catch (Exception ee)
            {

                return 0;
            }


        }
    }
}
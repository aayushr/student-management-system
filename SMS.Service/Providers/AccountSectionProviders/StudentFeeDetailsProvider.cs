﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SMS.Models.AccountSectionViewModel;
using SMS;
using SMS.SMS.Entities;
using System.Data.Entity;

namespace SMS.Service.Providers.AccountSectionProviders
{
    public interface IStudentFeeDetailsProvider
    {
        StudentFeeDetailsViewModel GetList();
        bool Save(StudentFeeDetailsViewModel model);
        StudentFeeDetailsViewModel Edit(int studentFeeDetailsId);
        bool Delete(int studentFeeDetailsId);
        bool StudentPaymentDetails(StudentFeeDetailsViewModel model);

    }
    public class StudentFeeDetailsProvider : IStudentFeeDetailsProvider
    {
        private readonly SMSEntities _ent;
        public StudentFeeDetailsProvider()
        {
            this._ent = new SMSEntities();
        }

        public StudentFeeDetailsViewModel GetList()
        {
            StudentFeeDetailsViewModel model = new StudentFeeDetailsViewModel();
            model.StudentFeeDetailsViewModelList = (from sf in _ent.StudentFeeDetails
                                                    join s in _ent.StudentInfoes on sf.StudentId equals s.StudentId
                                                    join c in _ent.SetupClasses on sf.ClassId equals c.ClassId
                                                    join f in _ent.SetupFaculties on sf.FacultyId equals f.FacultyId
                                                    join pc in _ent.SetupPaymentForClasses on sf.PaymentForClassId equals pc.PaymentForClassId
                                                    join pl in _ent.SetupPaymentForLibraries on sf.PaymentForLibraryId equals pl.PaymentForLibraryId
                                                    join ph in _ent.SetupPaymentForHostels on sf.PaymentForHostelId equals ph.PaymentForHostelId
                                                    join pa in _ent.SetupPaymentForActivities on sf.PaymentForActivitiesId equals pa.PaymentForActivitiesId
                                                    where sf.DeletedDate == null
                                                    select new StudentFeeDetailsViewModel
                                                    {
                                                        StudentFeeDetailsId = sf.StudentFeeDetailsId,
                                                        StudentName = s.FirstName + s.MidName + s.LastName,
                                                        Class = c.ClassName,
                                                        Faculty = f.FacultyName,
                                                        Library = pl.LibraryName,
                                                        Hostel = ph.HostelName,
                                                        Activities = pa.ActivitiesName,
                                                        PaymentForClassId = sf.PaymentForClassId,
                                                        PaidBy = sf.PaidBy,
                                                        Payment = sf.Payment ?? 0,
                                                        Relation = sf.Relation,
                                                        ReceivedBy = sf.ReceivedBy,
                                                        ReceivedDate = sf.ReceivedDate
                                                    }).ToList();
            return model;
        }

        public bool Save(StudentFeeDetailsViewModel model)
        {
            StudentFeeDetail studentFeeDetailEntity = new StudentFeeDetail();
            studentFeeDetailEntity.StudentFeeDetailsId = model.StudentFeeDetailsId;
            studentFeeDetailEntity.StudentId = model.StudentId;
            studentFeeDetailEntity.ClassId = model.ClassId;
            studentFeeDetailEntity.FacultyId = model.FacultyId;
            studentFeeDetailEntity.PaymentForClassId = model.PaymentForClassId;
            studentFeeDetailEntity.PaymentForLibraryId = model.PaymentForLibraryId;
            studentFeeDetailEntity.PaymentForHostelId = model.PaymentForHostelId;
            studentFeeDetailEntity.PaymentForActivitiesId = model.PaymentForActivitiesId;
            studentFeeDetailEntity.PaidBy = model.PaidBy;
            studentFeeDetailEntity.Payment = model.Payment;
            studentFeeDetailEntity.Relation = model.Relation;
            studentFeeDetailEntity.ReceivedBy = model.ReceivedBy;
            studentFeeDetailEntity.ReceivedDate = DateTime.UtcNow;


            if (model.StudentFeeDetailsId > 0)
            {
                studentFeeDetailEntity.Status = model.Status;
                studentFeeDetailEntity.UpdatedBy = 1;
                studentFeeDetailEntity.UpdatedDate = DateTime.UtcNow;
                _ent.Entry(studentFeeDetailEntity).State = EntityState.Modified;
                _ent.Entry(studentFeeDetailEntity).Property(x => x.CreatedBy).IsModified = false;
                _ent.Entry(studentFeeDetailEntity).Property(x => x.CreatedDate).IsModified = false;
            }
            else
            {
                studentFeeDetailEntity.Status = true;
                studentFeeDetailEntity.CreatedBy = 1;
                studentFeeDetailEntity.CreatedDate = DateTime.UtcNow;
                _ent.Entry(studentFeeDetailEntity).State = EntityState.Added;
            }
            _ent.SaveChanges();
            return true;
        }

        public StudentFeeDetailsViewModel Edit(int studentFeeDetailsId)
        {
            StudentFeeDetailsViewModel model = new StudentFeeDetailsViewModel();
            var data = _ent.StudentFeeDetails.Where(x => x.StudentFeeDetailsId == studentFeeDetailsId).FirstOrDefault();
            if (data != null)
            {
                data.StudentFeeDetailsId = model.StudentFeeDetailsId;
                data.StudentId = model.StudentId;
                data.ClassId = model.ClassId;
                data.FacultyId = model.FacultyId;
                data.PaymentForClassId = model.PaymentForClassId;
                data.PaymentForLibraryId = model.PaymentForLibraryId;
                data.PaymentForHostelId = model.PaymentForHostelId;
                data.PaymentForActivitiesId = model.PaymentForActivitiesId;
                data.PaidBy = model.PaidBy;
                data.Payment = model.Payment;
                data.Relation = model.Relation;
                data.ReceivedBy = model.ReceivedBy;
                data.Status = model.Status;
            }

            return model;
        }

        public bool Delete(int studentFeeDetailsId)
        {
            var data = _ent.StudentFeeDetails.Where(x => x.StudentFeeDetailsId == studentFeeDetailsId).FirstOrDefault();
            if (data != null)
            {
                data.DeletedBy = 1;
                data.DeletedDate = DateTime.UtcNow;
                _ent.Entry(data).State = EntityState.Modified;
            }
            _ent.SaveChanges();
            return true;
        }



        public bool StudentPaymentDetails(StudentFeeDetailsViewModel model)
        {

            var studentFeeDetails = new StudentFeeDetail();
            if (model.StudentId > 0 && model.Payment > 100)
            {
                var user = Userprovider.CustomeUserManager.GetUserDetails();
                studentFeeDetails.StudentId = model.StudentId;
                studentFeeDetails.ClassId = model.ClassId;
                studentFeeDetails.PaymentForClassId = model.PaymentForClassId;
                studentFeeDetails.FacultyId = model.FacultyId;
                studentFeeDetails.PaymentForLibraryId = model.PaymentForLibraryId;
                studentFeeDetails.PaymentForActivitiesId = model.PaymentForActivitiesId;
                studentFeeDetails.PaymentForHostelId = model.PaymentForHostelId;
                studentFeeDetails.Payment = model.Payment;
                studentFeeDetails.PaidBy = model.PaidBy;
                studentFeeDetails.Relation = model.Relation;
                studentFeeDetails.ReceivedBy = user.UserId;
                studentFeeDetails.ReceivedDate = DateTime.UtcNow;
                _ent.Entry(studentFeeDetails).State = EntityState.Added;
                _ent.SaveChanges();
            }
            return true;
        }


    }
}
